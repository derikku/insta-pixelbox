﻿using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace littlepixelbox.Models
{
    public class PolaroidModel
    {
        private readonly int _width;
        private readonly int _height;
        private readonly int _dpi;

        public PolaroidModel(int width, int height, int dpi)
        {
            _width = width;
            _height = height;
            _dpi = dpi;
        }
        public Brush BackgroundColor { get; set; }
        public BitmapSource BackgroundImage { get; set; }
        public bool BackgroundImageVisible { get; set; }
        public BitmapSource Overlay { get; set; }
        public bool OverlayVisible { get; set; }
        public TextModel Username { get; set; }
        public ImageModel LocationIcon { get; set; }
        public TextModel Location { get; set; }
        public ImageModel CaptionIcon { get; set; }
        public TextModel Caption { get; set; }
        public TextModel Branding { get; set; }
        public TextModel CustomText { get; set; }
        public TextModel Time { get; set; } 
        public ImageModel ProfileImage { get; set; }
        public ImageModel Image { get; set; }
        public ImageModel Logo { get; set; }
        
        public int GetWidth()
        {
            return _width;
        }
        public int GetHeight()
        {
            return _height;
        }

        public int GetDpi()
        {
            return _dpi;
        }
    }
}
