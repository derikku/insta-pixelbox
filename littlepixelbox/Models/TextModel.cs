﻿using System.Windows;
using System.Windows.Media;

namespace littlepixelbox.Models
{
    public class TextModel
    {
        public string Text { get; set; }
        public FontFamily FontFamily { get; set; }
        public FontStyle FontStyle { get; set; }
        public FontWeight FontWeight { get; set; }
        public FontStretch FontStretch { get; set; }
        public int FontSize { get; set; }
        public Brush Foreground { get; set;}
        public int Width { get; set; }
        public int Height { get; set; }
        public Point Point { get; set; }
        public bool Visible { get; set; }
    }
}
