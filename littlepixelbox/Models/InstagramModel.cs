﻿using System;

namespace littlepixelbox.Models
{
    public class InstagramModel
    {
       public string HashTag { get; set; }
       public string ClientId { get; set; }
       public DateTime DateOfPost { get; set; }
    }
}
