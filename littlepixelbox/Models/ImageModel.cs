﻿using System;
using System.Windows.Media.Imaging;

namespace littlepixelbox.Models
{
    public class ImageModel
    {
        public bool Visible { get; set; }
        public BitmapSource Source { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int Left { get; set; }
        public int Top { get; set; }
    }
}
