﻿using System;

namespace littlepixelbox.Models
{
    public class DataModel
    {
        public string MediaId
        {
            get;
            set;
        }
        public string Username
        {
            get;
            set;
        }
        public DateTime DateOfPost
        {
            get;
            set;
        }
        public int Downloaded
        {
            get;
            set;
        }
        public int Created
        {
            get;
            set;
        }
        public int Printed
        {
            get;
            set;
        }
        public int Collage
        {
            get;
            set;
        }
        public string MediaType
        {
            get;
            set;
        }
        public string ProfileImageUrl
        {
            get;
            set;
        }
        public string MediaUrl
        {
            get;
            set;
        }
        public string ProfileImageLocation
        {
            get;
            set;
        }
        public string MediaLocation
        {
            get;
            set;
        }
        public string CaptionTextLocation
        {
            get;
            set;
        }
        public string PolaroidLocation
        {
            get;
            set;
        }
    }
}
