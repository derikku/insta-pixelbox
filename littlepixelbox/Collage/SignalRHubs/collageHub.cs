﻿using System.Collections.Generic;
using System.Linq;
using Dapper;
using littlepixelbox.Core;
using littlepixelbox.Core.Singletons;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;

namespace littlepixelbox.Collage.SignalRHubs
{
    [HubName("collageHub")]
    public class CollageHub : Hub
    {
        public CollageHub()
        {
        }

        public void GetCollageSettings()
        {
            Clients.All.settings(CollageSingleton.Instance.ImgRow, CollageSingleton.Instance.ScrollSpeed);
        }

        public void GetImages()
        {
            List<string> images;

            using (var conn = Database.GetConnection())
            {
                const string queryString = "SELECT MediaLocation FROM Data WHERE Created = '1'";
                images = conn.Query<string>(queryString).ToList();
            }

            Clients.Client(GetClientId()).imageList(images, ReloadClients);
            Clients.All.statusCounters(images.Count, CollageSingleton.Instance.PrintCount, CollageSingleton.Instance.ErrorCount);

            ReloadClients = false;
        }

        private string GetClientId()
        {
            var clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        private static bool ReloadClients
        {
            get { return CollageSingleton.Instance.ReloadClients; }
            set { CollageSingleton.Instance.ReloadClients = value; }
        }
    }
}