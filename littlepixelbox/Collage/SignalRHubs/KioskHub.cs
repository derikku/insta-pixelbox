﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Dapper;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Models;
using littlepixelbox.Properties;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using NLog;

namespace littlepixelbox.Collage.SignalRHubs
{
    [HubName("kioskHub")]
    public class KioskHub : Hub
    {
        private readonly Logger _log = LogManager.GetLogger("Collage");

        private string _status;

        private PrintPolaroidService _pps;

        public KioskHub()
        {
            _status = string.Empty;

            _pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);
        }

        public override Task OnConnected()
        {
            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public void SelectedImages(IEnumerable<string> filenames)
        {
            SelectedImageSingleton.Instance.Images = filenames;
        }

        public void GetImages()
        {
            List<string> images;

            using (var conn = Database.GetConnection())
            {
                const string queryString = "SELECT PolaroidLocation FROM Data WHERE Created = '1'";
                images = conn.Query<string>(queryString).ToList();
            }

            Clients.Client(GetClientId()).imageList(images, ReloadClients);
            Clients.All.statusCounters(images.Count, CollageSingleton.Instance.WebRequestCount, CollageSingleton.Instance.PrintCount, CollageSingleton.Instance.ErrorCount);

            ReloadClients = false;
        }

        public void PrintImages(int copies)
        {
            _pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            try
            {
                foreach (var filename in SelectedImageSingleton.Instance.Images)
                {
                    var imgPath = GetPolaroidPath(filename);

                    _log.Info("Sending {0} to printer...", filename);

                    for (var i = 0; i < copies; i++)
                    {
                        _pps.PrintImageFromPath(imgPath);
                    }

                    _log.Info(string.Format("{0} photo(s) has been sent to printer.", copies));
                }

                _status = "Your photo(s) is now printing...";
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);

                _status =
                    string.Format(
                        "An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>",
                        ex.Message);
            }

            Clients.Client(GetClientId()).printStatus(_status);
        }

        public async Task EmailImages(string mailTo)
        {
            try
            {
                if (IsValidEmail(mailTo))
                {
                    using (var mail = new MailMessage())
                    {
                        var mailaddress = new MailAddress(AppSettings.Get<string>("MailServerEmailAddress"),
                            AppSettings.Get<string>("MailServerEmailName"));
                        mail.Subject = "Little Pixel Box";
                        mail.From = mailaddress;
                        mail.To.Add(new MailAddress(mailTo));

                        foreach (var filename in SelectedImageSingleton.Instance.Images)
                        {
                            var imgPath = GetPolaroidPath(filename);

                            var att = new Attachment(imgPath)
                            {
                                ContentType = {MediaType = "image/jpg"}
                            };

                            mail.Attachments.Add(att);
                        }

                        var body = new StringBuilder();

                        var template = Resources.email_template;

                        body.Append(template);

                        body.Replace("#LOGO_SOURCE#", AppSettings.Get<string>("EmailTemplate-LogoSource"));
                        body.Replace("#LOGO_LINK#", AppSettings.Get<string>("EmailTemplate-LogoLink"));
                        body.Replace("#CONTENT#",
                            AppSettings.Get<string>("EmailTemplate-Content").Replace(@"\n", "<br/>"));
                        body.Replace("#YEAR#", DateTime.Now.Year.ToString());
                        body.Replace("#USER_EMAIL#", mailTo);
                        body.Replace("#COMPANY_NAME#", AppSettings.Get<string>("EmailTemplate-CompanyName"));
                        body.Replace("#COMPANY_EMAIL#", AppSettings.Get<string>("EmailTemplate-CompanyEmailAddress"));

                        mail.Body = Regex.Replace(body.ToString(), @"\t|\n|\r", @"");
                        mail.IsBodyHtml = true;

                        using (
                            var smtp = new SmtpClient(AppSettings.Get<string>("MailServerAddress"),
                                AppSettings.Get<int>("MailServerPort")))
                        {
                            if (AppSettings.Get<bool>("MailServerAuthentication"))
                            {
                                var smtpUserInfo = new NetworkCredential(AppSettings.Get<string>("MailServerUsername"),
                                    AppSettings.Get<string>("MailServerPassword"));
                                smtp.UseDefaultCredentials = false;
                                smtp.Credentials = smtpUserInfo;
                                smtp.EnableSsl = AppSettings.Get<bool>("MailServerUseSSL");
                            }
                            _log.Info("Emailing photo(s) to {0}", mailTo);
                            await smtp.SendMailAsync(mail);

                            _log.Info("Email sent successfully.");
                            _status = string.Format("Email has been successfully sent to {0}", mailTo);
                        }
                    }
                }
                else
                {
                    _status = "Please enter a valid email address.";
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());

                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);

                _status =
                    string.Format(
                        "An error has occured.<br><br>Please notify one of the staff for assistance.<br><br><i>{0}</i>",
                        ex.Message);
            }

            Clients.Client(GetClientId()).emailStatus(_status);
        }

        public string GetPolaroidPath(string filename)
        {
            string result;

            using (var conn = Database.GetConnection())
            {
                const string queryString =
                    "SELECT PolaroidLocation FROM Data WHERE PolaroidLocation LIKE @file AND Downloaded = @downloaded";
                result = conn.Query<string>(queryString, new {file = "%" + filename, downloaded = 1}).FirstOrDefault();
            }

            return result;
        }

        private string GetClientId()
        {
            var clientId = "";
            if (Context.QueryString["clientId"] != null)
            {
                clientId = Context.QueryString["clientId"];
            }

            if (string.IsNullOrEmpty(clientId.Trim()))
            {
                clientId = Context.ConnectionId;
            }

            return clientId;
        }

        private static bool IsValidEmail(string email)
        {
            try
            {
                var addr = new MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private static bool ReloadClients
        {
            get { return CollageSingleton.Instance.ReloadClients; }
            set { CollageSingleton.Instance.ReloadClients = value; }
        }
    }
}