﻿using littlepixelbox.Core.Singletons;
using Nancy;
using Nancy.Conventions;

namespace littlepixelbox.Collage
{
    public class Bootstrapper : DefaultNancyBootstrapper
    {
        protected override void ConfigureConventions(NancyConventions nancyConventions)
        {
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory(@"/Collage/Content"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory(@"/Collage/Images"));
            nancyConventions.StaticContentsConventions.Add(StaticContentConventionBuilder.AddDirectory(DefaultPathSingleton.Instance.FolderPath.Replace(@"\", "/")));

            base.ConfigureConventions(nancyConventions);
        }
    }
}   
