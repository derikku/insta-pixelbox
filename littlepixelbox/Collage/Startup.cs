﻿using littlepixelbox.Collage;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Startup))]
namespace littlepixelbox.Collage
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/Collage/signalr", map =>
            {
                var hubConfiguration = new HubConfiguration()
                {
                    EnableDetailedErrors = true,

                    // Initial a new instance of DependencyResolve to support Signalr stop/start.
                    Resolver = new DefaultDependencyResolver()
                };
                map.RunSignalR(hubConfiguration);
            })
            .UseNancy();
        }
    }
}