﻿using Nancy;

namespace littlepixelbox.Collage.Modules
{
    public class CollageModule : NancyModule
    {
        public CollageModule()
        {
            Get["/collage"] = parameters => View["Collage/Views/Collage"];

            Get["/kiosk"] = parameters => View["Collage/Views/Kiosk"];
        }
    }
}
