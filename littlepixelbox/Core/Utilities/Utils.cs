﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using PixelFormat = System.Drawing.Imaging.PixelFormat;

namespace littlepixelbox.Core.Utilities
{
    internal class Utils
    {
        /// <summary>
        ///     Get local IP Address of computer
        /// </summary>
        /// <returns></returns>
        public static string LocalIpAddress()
        {
            IPHostEntry host;
            var localIP = string.Empty;
            host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    localIP = ip.ToString();
                }
            }
            return localIP;
        }

        /// <summary>
        ///     Covert Unix timestamp to DateTime.
        /// </summary>
        /// <param name="unixTimeStamp"></param>
        /// <returns></returns>
        public static DateTime UnixTimeStampToDateTime(int unixTimeStamp)
        {
            // Unix timestamp is seconds past epoch
            var dtDateTime = new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);
            dtDateTime = dtDateTime.AddSeconds(unixTimeStamp).ToLocalTime();
            return dtDateTime;
        }

        /// <summary>
        ///     Finds visual child of a specified parent.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="depObj"></param>
        /// <returns></returns>
        public static IEnumerable<T> FindVisualChildren<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
            {
                for (var i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    var child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                    {
                        yield return (T) child;
                    }

                    foreach (var childOfChild in FindVisualChildren<T>(child))
                    {
                        yield return childOfChild;
                    }
                }
            }
        }

        /// <summary>
        ///     Modify the Image color while ignoring transparency.
        /// </summary>
        /// <param name="image"></param>
        /// <param name="inColourR"></param>
        /// <param name="inColourG"></param>
        /// <param name="inColourB"></param>
        /// <returns></returns>
        public static BitmapImage ModifyBitmapColors(BitmapImage image, byte inColourR, byte inColourG, byte inColourB)
        {
            using (var outStream = new MemoryStream())
            {
                var enc = new PngBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(image));
                enc.Save(outStream);
                var bitmap = new Bitmap(outStream);

                using (var bmp = new Bitmap(outStream))
                {
                    // Specify a pixel format.
                    var pxf = PixelFormat.Format32bppArgb;

                    // Lock the bitmap's bits.
                    var rect = new Rectangle(0, 0, bmp.Width, bmp.Height);
                    var bmpData = bmp.LockBits(rect, ImageLockMode.ReadWrite, pxf);

                    // Get the address of the first line.
                    var ptr = bmpData.Scan0;

                    // Declare an array to hold the bytes of the bitmap. 
                    //int numBytes = (bmp.Width * 4) * bmp.Height; 
                    var numBytes = bmpData.Stride*bmp.Height;
                    var rgbValues = new byte[numBytes];

                    // Copy the RGB values into the array.
                    Marshal.Copy(ptr, rgbValues, 0, numBytes);

                    // Manipulate the bitmap
                    for (var counter = 0; counter < rgbValues.Length; counter += 4)
                    {
                        //if (rgbValues[counter] != 0 &&
                        //    rgbValues[counter + 1] != 0 &&
                        //    rgbValues[counter + 2] != 0)
                        if (rgbValues[counter + 3] != 0)
                        {
                            rgbValues[counter] = inColourB;
                            rgbValues[counter + 1] = inColourG;
                            rgbValues[counter + 2] = inColourR;
                        }
                    }

                    // Copy the RGB values back to the bitmap
                    Marshal.Copy(rgbValues, 0, ptr, numBytes);

                    // Unlock the bits.
                    bmp.UnlockBits(bmpData);

                    using (var memory = new MemoryStream())
                    {
                        bmp.Save(memory, ImageFormat.Png);
                        memory.Position = 0;

                        var bm = new BitmapImage();
                        bm.BeginInit();
                        bm.StreamSource = memory;
                        bm.CacheOption = BitmapCacheOption.OnLoad;
                        bm.EndInit();

                        return bm;
                    }
                }
            }
        }

        /// <summary>
        ///     Create and return BitmapImage from Uri and dispose it
        /// </summary>
        /// <param name="imageFile"></param>
        /// <returns></returns>
        public static BitmapImage LoadImageUri(Uri imageFile)
        {
            BitmapImage bitmap = null;
            if (imageFile != null)
            {
                var image = new BitmapImage();
                image.BeginInit();
                image.CacheOption = BitmapCacheOption.OnLoad;
                image.UriSource = imageFile;
                image.EndInit();

                bitmap = image;
            }
            return bitmap;
        }

        /// <summary>
        ///     Check if file is being processed by another thread
        /// </summary>
        /// <param name="file"></param>
        /// <returns></returns>
        public static bool IsFileLocked(FileInfo file)
        {
            FileStream stream = null;

            try
            {
                stream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
            }
            catch (IOException ex)
            {
                //the file is unavailable because it is:
                //still being written to
                //or being processed by another thread
                //or does not exist (has already been processed)
                var asd = ex.ToString();
                return true;
            }
            finally
            {
                if (stream != null)
                    stream.Close();
            }

            //file is not locked
            return false;
        }

        public static string RemoveLineEndings(string value)
        {
            if (string.IsNullOrEmpty(value))
            {
                return value;
            }
            var lineSeparator = ((char) 0x2028).ToString();
            var paragraphSeparator = ((char) 0x2029).ToString();

            return
                value.Replace("\r\n", " ")
                    .Replace("\n", " ")
                    .Replace("\r", " ")
                    .Replace(lineSeparator, " ")
                    .Replace(paragraphSeparator, " ");
        }


        public static Bitmap BitmapFromSource(BitmapSource bitmapsource)
        {
            Bitmap bitmap;

            using (var outStream = new MemoryStream())
            {
                BitmapEncoder enc = new BmpBitmapEncoder();
                enc.Frames.Add(BitmapFrame.Create(bitmapsource));
                enc.Save(outStream);
                bitmap = new Bitmap(outStream);
            }
            return bitmap;
        }
    }
}