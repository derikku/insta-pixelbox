﻿
namespace littlepixelbox.Core.Wrapper
{
    public class StringValue
    {
        string _value;

        public StringValue(string s)
        {
            _value = s;
        }

        public string Value
        {
            get { return _value; }
            set { _value = value; }
        }
    }
}
