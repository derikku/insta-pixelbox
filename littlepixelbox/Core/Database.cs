﻿using System;
using System.Data.SqlServerCe;
using System.IO;
using System.Reflection;
using Dapper;
using littlepixelbox.Core.Singletons;

namespace littlepixelbox.Core
{
    internal class Database
    {
        private static readonly string AssemblyPath = Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);

        public static SqlCeConnection GetConnection()
        {
            var folderPath = DefaultPathSingleton.Instance.FolderPath;
            var dbPath = Path.Combine(AssemblyPath, Path.Combine(folderPath, "Data.sdf"));
            var connString = string.Format("DataSource=\"{0}\";", dbPath);

            if (!string.IsNullOrEmpty(folderPath) && !File.Exists(dbPath))
            {
                CreateDatabase();
            }

            SqlCeConnection conn;

            try
            {
                conn = new SqlCeConnection(connString);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return conn;
        }

        public static void CreateDatabase()
        {
            var folderPath = DefaultPathSingleton.Instance.FolderPath;

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var dbPath = Path.Combine(folderPath, "Data.sdf");

            if (!File.Exists(dbPath))
            {
                var connString = string.Format("DataSource=\"{0}\";", dbPath);

                using (var en = new SqlCeEngine(connString))
                {
                    en.CreateDatabase();
                }

                const string queryString = "CREATE TABLE Data (" +
                                           "[MediaId] [nvarchar](50) PRIMARY KEY, " +
                                           "[Username] [nvarchar](50), " +
                                           "[DateOfPost] [datetime], " +
                                           "[Downloaded] [int], " +
                                           "[Created] [int], " +
                                           "[Printed] [int], " +
                                           "[Exported] [int], " +
                                           "[MediaType] [nvarchar](50), " +
                                           "[ProfileImageUrl] [nvarchar](200), " +
                                           "[MediaUrl] [nvarchar](200), " +
                                           "[ProfileImageLocation] [nvarchar](200), " +
                                           "[MediaLocation] [nvarchar](200), " +
                                           "[CaptionTextLocation] [nvarchar](200), " +
                                           "[PolaroidLocation] [nvarchar](200))";

                using (var conn = new SqlCeConnection(connString))
                {
                    conn.Query<int>(queryString);
                }
            }
        }


        public static SqlCeConnection GetBanListConnection()
        {
            const string folderPath = "Instagram";
            var dbPath = Path.Combine(AssemblyPath, Path.Combine(folderPath, "BanList.sdf"));
            var connString = string.Format("DataSource=\"{0}\";", dbPath);

            SqlCeConnection conn;

            try
            {
                conn = new SqlCeConnection(connString);
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return conn;
        }

        public static void CreateBanListDatabase()
        {
            const string folderPath = "Instagram";

            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            var dbPath = Path.Combine(folderPath, "BanList.sdf");

            if (!File.Exists(dbPath))
            {
                var connString = string.Format("DataSource=\"{0}\";", dbPath);

                using (var en = new SqlCeEngine(connString))
                {
                    en.CreateDatabase();
                }

                const string queryString = "CREATE TABLE BanList (" +
                                           "[Username] [nvarchar](50))";

                using (var conn = new SqlCeConnection(connString))
                {                  
                    conn.Query<int>(queryString);
                }
            }
        }
    }
}