﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Printing;
using System.Windows.Forms;
using System.Windows.Media.Imaging;
using Dapper;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Models;
using littlepixelbox.Properties;
using NLog;

namespace littlepixelbox.Core
{
    public class PrintPolaroidService
    {
        private readonly Logger _log = LogManager.GetLogger("PrintingService");

        private readonly PrintDocument _printdocument;
        private readonly float _pageWidth;
        private readonly float _pageHeight;
        private readonly int _marginX;
        private readonly int _marginY;

        public PrintPolaroidService(PrinterSettings settings, int marginX, int marginY)
        {
            PrintController printController = new StandardPrintController();

            _printdocument = new PrintDocument
            {
                PrintController = printController,
                PrinterSettings = settings
            };

            _pageWidth = _printdocument.DefaultPageSettings.Landscape
                ? _printdocument.DefaultPageSettings.PrintableArea.Height
                : _printdocument.DefaultPageSettings.PrintableArea.Width;
            _pageHeight = _printdocument.DefaultPageSettings.Landscape
                ? _printdocument.DefaultPageSettings.PrintableArea.Width
                : _printdocument.DefaultPageSettings.PrintableArea.Height;

            _marginX = marginX != 0 ? marginX : 10;
            _marginY = marginY != 0 ? marginY : 10;
        }

        public void PrintCreatedImages()
        {
            try
            {
                List<DataModel> data;
                using (var conn = Database.GetConnection())
                {
                    const string queryString = "SELECT * FROM Data WHERE Printed = 0 and Created = 1";
                    data = conn.Query<DataModel>(queryString, new {}).ToList();
                }

                foreach (var d in data)
                {
                    if (!Utils.IsFileLocked(new FileInfo(d.PolaroidLocation)))
                    {
                        var imgPath = d.PolaroidLocation;

                        _log.Info("Printing {0}...", imgPath);
                        PrintImageFromPath(imgPath);

                        using (var conn = Database.GetConnection())
                        {
                            var queryString = "UPDATE Data SET Printed = 1 WHERE MediaId = @MediaId";
                            conn.Query<DataModel>(queryString, new {d.MediaId});
                        }

                        _log.Info("Sent [{0}] to print queue.", imgPath);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        public void PrintImageFromPath(string filePath)
        {
            if (PrinterStatusOk(_printdocument.PrinterSettings.PrinterName))
            {                
                var x = _marginX / 2;
                var y = _marginY / 2;
                var width = _pageWidth - _marginX;
                var height = _pageHeight - _marginY;

                _printdocument.DocumentName = Path.GetFileName(filePath);
                _printdocument.PrintPage +=
                    (sender, args) =>
                    {
                        BitmapSource bitmap = Utils.LoadImageUri(new Uri(filePath.Replace(@"\", "/"), UriKind.Relative));                        
                        args.Graphics.DrawImage(Utils.BitmapFromSource(bitmap), x, y, width, height);
                    };

                try
                {
                    _printdocument.Print();

                    Mediator.NotifyColleagues("PrintCount", (int) _printdocument.PrinterSettings.Copies);
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Unable to print: {0}", ex));
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            }
        }

        public void PrintImage(string filename, Image img)
        {
            if (PrinterStatusOk(_printdocument.PrinterSettings.PrinterName))
            {
                var x = _marginX / 2;
                var y = _marginY / 2;
                var width = _pageWidth - _marginX;
                var height = _pageHeight - _marginY;

                _printdocument.DocumentName = filename;
                _printdocument.PrintPage +=
                    (sender, args) =>
                    {
                        args.Graphics.DrawImage(img, x, y, width, height);
                    };

                try
                {
                    _printdocument.Print();
                    img.Dispose();

                    Mediator.NotifyColleagues("PrintCount", (int)_printdocument.PrinterSettings.Copies);
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Unable to print: {0}", ex));
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            }
        }

        private bool PrinterStatusOk(string printerName)
        {
            var isOk = true;

            var ps = new PrintServer();
            ps.GetPrintQueues(new[] {EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections});

            var pq = new PrintQueue(ps, printerName);

            #region Check printer status

            if (pq.HasPaperProblem)
            {
                isOk = false;
                _log.Error("Printer has paper problems.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer has paper problems.");
            }
            else if (pq.IsInError)
            {
                isOk = false;
                _log.Error("Printer is in error.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is in error.");
            }
            else if (pq.IsPaperJammed)
            {
                isOk = false;
                _log.Error("Printer is paper jammed.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is paper jammed.");
            }
            else if (pq.IsPaused)
            {
                isOk = false;
                _log.Error("Printer is paused.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is paused.");
            }
            else if (pq.IsOffline)
            {
                isOk = false;
                _log.Error("Printer is offline.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is offline.");
            }
            else if (pq.IsOutOfMemory)
            {
                isOk = false;
                _log.Error("Printer is out of memory.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is out of memory.");
            }
            else if (pq.IsDoorOpened)
            {
                isOk = false;
                _log.Error("Printer door is opened.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer door is opened.");
            }
            else if (pq.IsTonerLow)
            {
                isOk = false;
                _log.Error("Printer toner low.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer toner low.");
            }
            else if (pq.IsOutOfPaper)
            {
                isOk = false;
                _log.Error("Printer is out of paper.");
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", "Printer is out of paper.");
            }
                #endregion

            return isOk;
        }

        public void PrintPreview(Image img)
        {
            _printdocument.DocumentName = "Little Pixel Box Test Print";
            _printdocument.PrintPage +=
                (sender, args) =>
                {
                    args.Graphics.DrawImage(img, _marginX/2, _marginY/2, _pageWidth - _marginX, _pageHeight - _marginY);
                };

            using (var ppd = new PrintPreviewDialog())
            {
                ppd.Icon = Resources.icon;
                ppd.Document = _printdocument;

                const int docHeight = 800;
                const int docWidth = 600;
                ppd.Height = _pageWidth > _pageHeight ? docWidth : docHeight;
                ppd.Width = _pageWidth > _pageHeight ? docHeight : docWidth;

                ((Form) ppd).StartPosition = FormStartPosition.CenterParent;
                ppd.ShowDialog();
            }
        }
    }
}
