﻿namespace littlepixelbox.Core.Singletons
{
    public sealed class InstagramAccessTokenSingleton
    {
        private static InstagramAccessTokenSingleton _instance;
        private static readonly object Padlock = new object();

        private InstagramAccessTokenSingleton()
        {
            AccessToken = string.Empty;
        }

        public static InstagramAccessTokenSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new InstagramAccessTokenSingleton());
                }
            }
        }

        public string AccessToken { get; set; }
    }
}
