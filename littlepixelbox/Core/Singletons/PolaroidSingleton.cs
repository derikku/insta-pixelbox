﻿using littlepixelbox.Models;

namespace littlepixelbox.Core.Singletons
{
    public sealed class PolaroidSingleton
    {
        private static PolaroidSingleton _instance;
        private static readonly object Padlock = new object();

        private PolaroidSingleton()
        {
        }

        public static PolaroidSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new PolaroidSingleton());
                }
            }
        }

        public PolaroidModel PolaroidModel { get; set; }
    }
}