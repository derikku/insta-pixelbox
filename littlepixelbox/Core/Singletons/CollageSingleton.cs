﻿
namespace littlepixelbox.Core.Singletons
{
    public sealed class CollageSingleton
    {
        private static CollageSingleton _instance;
        private static readonly object Padlock = new object();

        private CollageSingleton()
        {
            ReloadClients = false;
            WebRequestCount = 0;
            PrintCount = 0;
            ErrorCount = 0;
        }

        public static CollageSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new CollageSingleton());
                }
            }
        }

        public int ImgRow { get; set; }

        public int ScrollSpeed { get; set; }

        public bool ReloadClients { get; set; }

        public int WebRequestCount { get; set; }

        public int PrintCount { get; set; }

        public int ErrorCount { get; set; }
    }
}
