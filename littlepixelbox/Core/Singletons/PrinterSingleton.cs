﻿using System.Drawing.Printing;

namespace littlepixelbox.Core.Singletons
{
    public sealed class PrinterSingleton
    {
        private static PrinterSingleton _instance;
        private static readonly object Padlock = new object();
        private int _marginX = 10;
        private int _marginY = 10;

        private PrinterSettings _printerSettings = new PrinterSettings();

        private PrinterSingleton()
        {
        }

        public static PrinterSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new PrinterSingleton());
                }
            }
        }

        public PrinterSettings PrinterSettings
        {
            get { return _printerSettings; }
            set { _printerSettings = value; }
        }

        public int MarginX
        {
            get { return _marginX; }
            set { _marginX = value; }
        }

        public int MarginY
        {
            get { return _marginY; }
            set { _marginY = value; }
        }
    }
}