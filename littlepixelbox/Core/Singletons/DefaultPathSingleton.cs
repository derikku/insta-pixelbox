﻿

namespace littlepixelbox.Core.Singletons
{
    public sealed class DefaultPathSingleton
    {
        private static DefaultPathSingleton _instance;
        private static readonly object Padlock = new object();

        private DefaultPathSingleton()
        {
            FolderPath = string.Empty;
        }

        public static DefaultPathSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new DefaultPathSingleton());
                }
            }
        }

        public string FolderPath { get; set; }
    }
}
