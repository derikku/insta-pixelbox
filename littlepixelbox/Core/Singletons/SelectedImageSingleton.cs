﻿using System.Collections.Generic;

namespace littlepixelbox.Core.Singletons
{
    public sealed class SelectedImageSingleton
    {
        private static SelectedImageSingleton _instance;
        private static readonly object Padlock = new object();

        private SelectedImageSingleton()
        {
            Image = string.Empty;
        }

        public static SelectedImageSingleton Instance
        {
            get
            {
                lock (Padlock)
                {
                    return _instance ?? (_instance = new SelectedImageSingleton());
                }
            }
        }

        public string Image { get; set; }
        public IEnumerable<string> Images { get; set; }
    }
}
