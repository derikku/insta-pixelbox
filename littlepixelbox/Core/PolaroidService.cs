﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Dapper;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Models;
using Newtonsoft.Json.Linq;
using NLog;

namespace littlepixelbox.Core
{
    public class PolaroidService
    {
        private readonly PolaroidModel _polaroid;
        private readonly string _saveFolderPath;
        private readonly Logger _log = LogManager.GetLogger("PolaroidService");

        public PolaroidService(PolaroidModel polaroid)
        {
            _polaroid = polaroid;
            _saveFolderPath = Path.Combine(DefaultPathSingleton.Instance.FolderPath, @"Polaroids");
        }

        public void CreatePolaroid()
        {
            Directory.CreateDirectory(_saveFolderPath);

            CreatePolaroidImage();
            CreatePolaroidVideo();
        }

        private void CreatePolaroidImage()
        {
            try
            {
                List<DataModel> data;

                using (var conn = Database.GetConnection())
                {
                    const string queryString = "SELECT * FROM Data WHERE Created = 0 and Downloaded = 1 and MediaType = @mediaType";
                    data = conn.Query<DataModel>(queryString, new {mediaType = "image"}).ToList();
                }

                foreach (var d in data)
                {
                    if (!Utils.IsFileLocked(new FileInfo(d.MediaLocation)) &&
                        !Utils.IsFileLocked(new FileInfo(d.ProfileImageLocation)) &&
                        !Utils.IsFileLocked(new FileInfo(d.CaptionTextLocation)))
                    {
                        using (var reader = new StreamReader(d.CaptionTextLocation))
                        {
                            var captionText = reader.ReadLine();
                            reader.Close();

                            _log.Info("\t[Username:] " + d.Username);
                            _log.Info("\t[Caption Text:] " + captionText);

                            BitmapSource instagramImage = Utils.LoadImageUri(new Uri(d.MediaLocation, UriKind.Relative));
                            BitmapSource profileImage =
                                Utils.LoadImageUri(new Uri(d.ProfileImageLocation, UriKind.Relative));

                            _log.Info("Creating Image...");

                            try
                            {
                                BitmapSource bitmap = CreateBitmap(d.Username, captionText, profileImage, instagramImage);

                                var fileFullPath = Path.Combine(_saveFolderPath,
                                    string.Format("{0}_{1}.jpg", d.Username, d.MediaId));
                                WriteJpeg(fileFullPath, bitmap);
                                _log.Info("Image saved. {0}_{1}.jpg", d.Username, d.MediaId);

                                using (var conn = Database.GetConnection())
                                {
                                    var queryString =
                                        "UPDATE Data SET Created = 1, PolaroidLocation = @PolaroidLocation WHERE MediaId = @mediaId";
                                    conn.Query<DataModel>(queryString, new
                                    {
                                        PolaroidLocation = fileFullPath,
                                        mediaId = d.MediaId
                                    });
                                }
                                Mediator.NotifyColleagues("ImageCount", 1);
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex.ToString());
                                Mediator.NotifyColleagues("ErrorCount", 1);
                                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void CreatePolaroidVideo()
        {
            try
            {
                List<DataModel> data;

                using (var conn = Database.GetConnection())
                {
                    const string queryString = "SELECT * FROM Data WHERE Created = 0 and Downloaded = 1 and MediaType = @mediaType";
                    data = conn.Query<DataModel>(queryString, new {mediaType = "video"}).ToList();
                }

                foreach (var d in data)
                {
                    if (!Utils.IsFileLocked(new FileInfo(d.MediaLocation)) &&
                        !Utils.IsFileLocked(new FileInfo(d.ProfileImageLocation)) &&
                        !Utils.IsFileLocked(new FileInfo(d.CaptionTextLocation)))
                    {
                        using (var reader = new StreamReader(d.CaptionTextLocation))
                        {
                            var captionText = reader.ReadLine();
                            reader.Close();

                            _log.Info("\t[Username:] " + d.Username);
                            _log.Info("\t[Caption Text:] " + captionText);

                            var profileImage = Utils.LoadImageUri(new Uri(d.ProfileImageLocation, UriKind.Relative));

                            _log.Info("Creating Image...");

                            try
                            {
                                using (var stream = GetSnapshotsFromVideo(d.MediaLocation))
                                {
                                    var videoImg = new BitmapImage();
                                    videoImg.BeginInit();
                                    videoImg.CacheOption = BitmapCacheOption.OnLoad;
                                    videoImg.StreamSource = stream;
                                    videoImg.EndInit();

                                    var videoImgFullPath = string.Format("{0}\\{1}.jpg", d.MediaLocation.Substring(0, d.MediaLocation.LastIndexOf("\\", StringComparison.Ordinal)), d.MediaId);
                                    WriteJpeg(videoImgFullPath, videoImg);

                                    BitmapSource bitmap = CreateBitmap(d.Username, captionText, profileImage, videoImg);

                                    var imgFullPath = Path.Combine(_saveFolderPath, string.Format("{0}_{1}.jpg", d.Username, d.MediaId));
                                    WriteJpeg(imgFullPath, bitmap);

                                    _log.Info("Image saved. {0}_{1}.jpg", d.Username, d.MediaId);

                                    using (var conn = Database.GetConnection())
                                    {
                                        const string queryString = "UPDATE Data SET Created = @created, PolaroidLocation = @polaroidLocation WHERE MediaId = @mediaId";
                                        conn.Query<DataModel>(queryString, new
                                        {
                                            created = 1,
                                            polaroidLocation = imgFullPath,
                                            mediaId = d.MediaId
                                        });
                                    }
                                    Mediator.NotifyColleagues("VideoCount", 1);
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(ex.ToString());
                                Mediator.NotifyColleagues("ErrorCount", 1);
                                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        /// <summary>
        ///     Render the visual to bitmap
        /// </summary>
        /// <param name="username"></param>
        /// <param name="caption"></param>
        /// <param name="profileImage"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        public RenderTargetBitmap CreateBitmap(string username, string caption, BitmapSource profileImage, BitmapSource image)
        {
            var resolutionScale = (double) _polaroid.GetDpi()/96;

            var bitmap = new RenderTargetBitmap(
                (int) (resolutionScale*_polaroid.GetWidth()),
                (int) (resolutionScale*_polaroid.GetHeight()),
                resolutionScale*96,
                resolutionScale*96,
                PixelFormats.Default);

            bitmap.Render(DrawVisual(username, caption, profileImage, image));

            return bitmap;
        }

        /// <summary>
        ///     Draw polaroid onto DrawingVisual
        /// </summary>
        /// <param name="username"></param>
        /// <param name="caption"></param>
        /// <param name="profileImage"></param>
        /// <param name="image"></param>
        /// <returns></returns>
        private DrawingVisual DrawVisual(string username, string caption, ImageSource profileImage, ImageSource image)
        {
            var dv = new DrawingVisual();

            using (var dc = dv.RenderOpen())
            {
                dc.DrawImage(BitmapSourceFromBrush(_polaroid.BackgroundColor),
                    new Rect(0, 0, _polaroid.GetWidth(), _polaroid.GetHeight()));

                if (_polaroid.BackgroundImageVisible)
                {
                    dc.DrawImage(_polaroid.BackgroundImage, new Rect(0, 0, _polaroid.GetWidth(), _polaroid.GetHeight()));
                }

                dc.DrawImage(image,
                    new Rect(_polaroid.Image.Left, _polaroid.Image.Top, _polaroid.Image.Width, _polaroid.Image.Height));

                if (_polaroid.ProfileImage.Visible)
                {
                    dc.DrawImage(profileImage,
                        new Rect(_polaroid.ProfileImage.Left, _polaroid.ProfileImage.Top, _polaroid.ProfileImage.Width,
                            _polaroid.ProfileImage.Height));
                }

                if (_polaroid.Logo.Visible)
                {
                    dc.DrawImage(_polaroid.Logo.Source,
                        new Rect(_polaroid.Logo.Left, _polaroid.Logo.Top, _polaroid.Logo.Width, _polaroid.Logo.Height));
                }

                if (_polaroid.Username.Visible)
                {
                    var ft = new FormattedText(username, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                        new Typeface(_polaroid.Username.FontFamily, _polaroid.Username.FontStyle,
                            _polaroid.Username.FontWeight, _polaroid.Username.FontStretch), _polaroid.Username.FontSize,
                        _polaroid.Username.Foreground)
                    {
                        MaxTextWidth = _polaroid.Username.Width,
                        MaxTextHeight = _polaroid.Username.Height
                    };

                    dc.DrawText(ft, _polaroid.Username.Point);
                }

                if (_polaroid.LocationIcon.Visible)
                {
                    dc.DrawImage(_polaroid.LocationIcon.Source,
                        new Rect(_polaroid.LocationIcon.Left, _polaroid.LocationIcon.Top, _polaroid.LocationIcon.Width,
                            _polaroid.LocationIcon.Height));
                }

                if (_polaroid.Location.Visible)
                {
                    var ft = new FormattedText(_polaroid.Location.Text, CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface(_polaroid.Location.FontFamily, _polaroid.Location.FontStyle,
                            _polaroid.Location.FontWeight, _polaroid.Location.FontStretch), _polaroid.Location.FontSize,
                        _polaroid.Location.Foreground)
                    {
                        MaxTextWidth = _polaroid.Location.Width,
                        MaxTextHeight = _polaroid.Location.Height
                    };

                    dc.DrawText(ft, _polaroid.Location.Point);
                }

                if (_polaroid.CaptionIcon.Visible)
                {
                    dc.DrawImage(_polaroid.CaptionIcon.Source,
                        new Rect(_polaroid.CaptionIcon.Left, _polaroid.CaptionIcon.Top, _polaroid.CaptionIcon.Width,
                            _polaroid.CaptionIcon.Height));
                }

                if (_polaroid.Caption.Visible)
                {
                    var ft = new FormattedText(caption, CultureInfo.GetCultureInfo("en-us"), FlowDirection.LeftToRight,
                        new Typeface(_polaroid.Caption.FontFamily, _polaroid.Caption.FontStyle,
                            _polaroid.Caption.FontWeight, _polaroid.Caption.FontStretch), _polaroid.Caption.FontSize,
                        _polaroid.Caption.Foreground)
                    {
                        MaxTextWidth = _polaroid.Caption.Width,
                        MaxTextHeight = _polaroid.Caption.Height
                    };
                    dc.DrawText(ft, _polaroid.Caption.Point);
                }

                if (_polaroid.Branding.Visible)
                {
                    var ft = new FormattedText(_polaroid.Branding.Text, CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface(_polaroid.Branding.FontFamily, _polaroid.Branding.FontStyle,
                            _polaroid.Branding.FontWeight, _polaroid.Branding.FontStretch), _polaroid.Branding.FontSize,
                        _polaroid.Branding.Foreground)
                    {
                        MaxTextWidth = _polaroid.Branding.Width,
                        MaxTextHeight = _polaroid.Branding.Height
                    };

                    dc.DrawText(ft, _polaroid.Branding.Point);
                }

                if (_polaroid.CustomText.Visible)
                {
                    var ft = new FormattedText(_polaroid.CustomText.Text, CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface(_polaroid.CustomText.FontFamily, _polaroid.CustomText.FontStyle,
                            _polaroid.CustomText.FontWeight, _polaroid.CustomText.FontStretch),
                        _polaroid.CustomText.FontSize, _polaroid.CustomText.Foreground)
                    {
                        MaxTextWidth = _polaroid.CustomText.Width,
                        MaxTextHeight = _polaroid.CustomText.Height
                    };

                    dc.DrawText(ft, _polaroid.CustomText.Point);
                }

                if (_polaroid.Time.Visible)
                {
                    var ft = new FormattedText(string.Format("{0:hh:mm tt}", DateTime.Now), CultureInfo.GetCultureInfo("en-us"),
                        FlowDirection.LeftToRight,
                        new Typeface(_polaroid.Time.FontFamily, _polaroid.Time.FontStyle,
                            _polaroid.Time.FontWeight, _polaroid.Time.FontStretch),
                        _polaroid.Time.FontSize, _polaroid.Time.Foreground)
                    {
                        MaxTextWidth = _polaroid.Time.Width,
                        MaxTextHeight = _polaroid.Time.Height
                    };

                    dc.DrawText(ft, _polaroid.Time.Point);
                }

                if (_polaroid.OverlayVisible)
                {
                    dc.DrawImage(_polaroid.Overlay, new Rect(0, 0, _polaroid.GetWidth(), _polaroid.GetHeight()));
                }
            }

            return dv;
        }

        /// <summary>
        ///     Converts Brush to BitmapSource
        /// </summary>
        /// <param name="drawingBrush"></param>
        /// <param name="size"></param>
        /// <param name="dpi"></param>
        /// <returns></returns>
        private static BitmapSource BitmapSourceFromBrush(Brush drawingBrush, int size = 32, int dpi = 96)
        {
            // RenderTargetBitmap = builds a bitmap rendering of a visual
            var pixelFormat = PixelFormats.Pbgra32;
            var rtb = new RenderTargetBitmap(size, size, dpi, dpi, pixelFormat);

            // Drawing visual allows us to compose graphic drawing parts into a visual to render
            var drawingVisual = new DrawingVisual();
            using (var context = drawingVisual.RenderOpen())
            {
                // Declaring drawing a rectangle using the input brush to fill up the visual
                context.DrawRectangle(drawingBrush, null, new Rect(0, 0, size, size));
            }

            // Actually rendering the bitmap
            rtb.Render(drawingVisual);
            return rtb;
        }

        /// <summary>
        ///     Save BitmapSource to PNG
        /// </summary>
        /// <param name="fileFullPath"></param>
        /// <param name="bitmapSource"></param>
        public void WriteJpeg(string fileFullPath, BitmapSource bitmapSource)
        {
            var encoder = new JpegBitmapEncoder();
            var outputFrame = BitmapFrame.Create(bitmapSource);
            encoder.Frames.Add(outputFrame);

            using (var file = File.OpenWrite(fileFullPath))
            {
                encoder.Save(file);
            }
        }

        public string ExecuteCmd(string exePath, string parameters)
        {
            string result;

            using (var p = new Process())
            {
                p.StartInfo.UseShellExecute = false;
                p.StartInfo.CreateNoWindow = true;
                p.StartInfo.RedirectStandardOutput = true;
                p.StartInfo.RedirectStandardError = true;
                p.StartInfo.FileName = exePath;
                p.StartInfo.Arguments = parameters;
                p.Start();

                result = exePath.Equals("ffmpeg.exe") ? p.StandardError.ReadToEnd() : p.StandardOutput.ReadToEnd();
                p.WaitForExit();
            }
            return result;
        }

        /// <summary>
        ///     Get the duration of the video using ffprobe and return a list of timeframes
        /// </summary>
        /// <param name="videoFile"></param>
        /// <returns></returns>
        private IEnumerable<TimeSpan> GetVidDuration(string videoFile)
        {
            var list = new List<TimeSpan>();

            var cmdParams = string.Format("-v quiet -print_format json -show_format {0}", videoFile);
            try
            {
                var result = ExecuteCmd(@"Tools\ffprobe.exe", cmdParams);

                JToken token = JObject.Parse(result);
                var duration = token.SelectToken("format").SelectToken("duration").ToString();

                var dura = Convert.ToDouble(duration)/4;
                var frame1 = dura - dura/2;
                var frame2 = dura*2 - dura/2;
                var frame3 = dura*3 - dura/2;
                var frame4 = dura*4 - dura/2;

                list.Add(TimeSpan.FromSeconds(frame1));
                list.Add(TimeSpan.FromSeconds(frame2));
                list.Add(TimeSpan.FromSeconds(frame3));
                list.Add(TimeSpan.FromSeconds(frame4));
            }
            catch (Exception ex)
            {
                list = null;
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }

            return list;
        }


        /// <summary>
        ///     Create the individual images base on the timeframes
        /// </summary>
        /// <param name="videoFile"></param>
        /// <returns></returns>
        private MemoryStream GetSnapshotsFromVideo(string videoFile)
        {
            if (videoFile.Contains(" "))
            {
                videoFile = "\"" + videoFile + "\"";
            }

            var timeFrames = GetVidDuration(videoFile);


            var images = new List<string>();

            var i = 1;

            foreach (var t in timeFrames)
            {
                var saveAsName = string.Format("{0}_{1}.jpg", Path.GetFileNameWithoutExtension(videoFile), i);

                var time = t.ToString(@"hh\:mm\:ss");

                var ffmpegParams = string.Format("-hide_banner -ss {0} -i {1} -r 1 -t 1 -f image2 {2}", time, videoFile,
                    saveAsName);

                try
                {
                    ExecuteCmd(@"Tools\ffmpeg.exe", ffmpegParams);
                    images.Add(saveAsName);
                }
                catch (Exception ex)
                {
                    _log.Error(ex.ToString());
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
                i++;
            }

            return CreateQuadImage(images);
        }

        /// <summary>
        ///     Create a 2x2 image with 4 images into memory
        /// </summary>
        /// <param name="images"></param>
        /// <returns></returns>
        private MemoryStream CreateQuadImage(List<string> images)
        {
            const int spacing = 2;
            var width = _polaroid.Image.Width/2 - 1;
            var height = _polaroid.Image.Height/2 - 1;
            var resolutionScale = (double)_polaroid.GetDpi()/96;

            var bitmap = new RenderTargetBitmap(
                (int) (resolutionScale*_polaroid.Image.Width),
                (int) (resolutionScale*_polaroid.Image.Height),
                resolutionScale*96,
                resolutionScale*96, PixelFormats.Default);

            var dv = new DrawingVisual();

            using (var dc = dv.RenderOpen())
            {
                var image1 = Utils.LoadImageUri(new Uri(images[0], UriKind.Relative));
                var image2 = Utils.LoadImageUri(new Uri(images[1], UriKind.Relative));
                var image3 = Utils.LoadImageUri(new Uri(images[2], UriKind.Relative));
                var image4 = Utils.LoadImageUri(new Uri(images[3], UriKind.Relative));

                dc.DrawImage(image1, new Rect(0, 0, width, height));
                dc.DrawImage(image2, new Rect(0, width + spacing, width, height));
                dc.DrawImage(image3, new Rect(height + spacing, 0, width, height));
                dc.DrawImage(image4, new Rect(width + spacing, height + spacing, width, height));
            }

            bitmap.Render(dv);

            var stream = new MemoryStream();
            var encoder = new PngBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bitmap));
            encoder.Save(stream);

            foreach (var img in images.Where(File.Exists))
            {
                File.Delete(img);
            }

            return stream;
        }
    }
}