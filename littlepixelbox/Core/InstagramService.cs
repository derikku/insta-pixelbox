﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using Dapper;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Models;
using Newtonsoft.Json.Linq;
using NLog;

namespace littlepixelbox.Core
{
    public class InstagramService
    {
        private readonly Logger _log = LogManager.GetLogger("InstagramService");

        private string _hashTag;
        private string _hashTag2;
        private readonly string _accessToken;
        private readonly string _folderPath;

        private readonly int _webRequestTimeout;

        private bool _download;

        private DateTime _startDate;
        private DateTime _endDate;

        public InstagramService(string accessToken, string downloadFolderPath, int webRequestTimeout)
        {
            _accessToken = accessToken;
            NextPageUrl = string.Empty;
            _folderPath = Path.Combine(downloadFolderPath, "Downloads");
            _webRequestTimeout = webRequestTimeout;
        }

        public void Start(string hashTag, string hashTag2, DateTime startDate, DateTime endDate, bool download)
        {
            _hashTag = hashTag;
            _hashTag2 = hashTag2;
            _startDate = startDate;
            _endDate = endDate;
            _download = download;

            _log.Info("Starting Instagram service...");
            IsRunning = true;

            do
            {
                try
                {
                    GetMedia(TagSearch(_hashTag));
                    Thread.Sleep(1000);
                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            } while (IsRunning);
        }

        public JObject TagSearch(string hashTag)
        {
            var request = (HttpWebRequest)WebRequest.Create(
                string.Format("https://api.instagram.com/v1/tags/{0}/media/recent?access_token={1}", hashTag, _accessToken));

            if (!string.IsNullOrEmpty(NextPageUrl))
            {
                request = (HttpWebRequest)WebRequest.Create(NextPageUrl);
                _log.Trace("Using next page URL: {0}", NextPageUrl);
            }

            return GetToken(request);
        }

        public JObject UserSearch(string username)
        {
            var request = (HttpWebRequest)WebRequest.Create(
                string.Format("https://api.instagram.com/v1/users/search?q={0}&access_token={1}", username, _accessToken));

            return GetToken(request);
        }

        public JObject UserImageSearch(string userId)
        {
            var request = (HttpWebRequest)WebRequest.Create(
                string.Format("https://api.instagram.com/v1/users/{0}/media/recent?access_token={1}", userId, _accessToken));


            if (!string.IsNullOrEmpty(NextPageUrl))
            {
                request = (HttpWebRequest)WebRequest.Create(NextPageUrl);
                _log.Trace("Using next page URL: {0}", NextPageUrl);
            }

            return GetToken(request);
        }

        public JObject GetToken(HttpWebRequest request)
        {
            JObject token = null;
            NextPageUrl = string.Empty;

            try
            {
                _log.Trace("Sending web request...");
                var responseStream = request.GetResponse().GetResponseStream();
                if (responseStream != null)
                {
                    responseStream.ReadTimeout = _webRequestTimeout;
                    responseStream.WriteTimeout = _webRequestTimeout;

                    Mediator.NotifyColleagues("WebRequestCount", 1);

                    using (var reader = new StreamReader(responseStream, Encoding.Default))
                    {
                        _log.Trace("Deserializing json object...");
                        token = JObject.Parse(reader.ReadToEnd());

                        if (token.SelectToken("pagination") != null)
                        {
                            if (token.SelectToken("pagination").SelectToken("next_url") != null)
                            {
                                NextPageUrl = token.SelectToken("pagination").SelectToken("next_url").ToString();
                                _log.Trace("Next page URL found! {0}", NextPageUrl);
                            }
                        }

                        _log.Trace("Reading json object...");
                        _log.Trace(token.ToString());
                    }
                }
            }
            catch (WebException ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }

            return token;
        }

        private void GetMedia(JToken token)
        {
            if (token != null)
            {
                var json = token.SelectToken("data").ToArray();

                foreach (var j in json)
                {
                    if (!IsRunning)
                    {
                        return;
                    }
                    var dateOfPost = Utils.UnixTimeStampToDateTime(j.SelectToken("created_time").Value<int>());


                    if (dateOfPost >= _startDate && dateOfPost <= _endDate)
                    {
                        var username = j.SelectToken("user").SelectToken("username").ToString();

                        List<string> userBanList;

                        var queryString = "SELECT * FROM BanList";
                        using (var conn = Database.GetBanListConnection())
                        {
                            userBanList = conn.Query<string>(queryString, new { }).ToList();
                        }

                        if (userBanList.Contains(username))
                        {
                            if (Directory.Exists(_folderPath))
                            {
                                var di = new DirectoryInfo(_folderPath);

                                foreach (var d in di.EnumerateDirectories().Where(d => userBanList.Contains(d.Name)))
                                {
                                    d.Delete(true);
                                    CollageSingleton.Instance.ReloadClients = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            var profileImageUrl = j.SelectToken("user").SelectToken("profile_picture").ToString();
                            var mediaId = j.SelectToken("id").ToString();
                            var mediaType = j.SelectToken("type").ToString();
                            var mediaUrl = string.Empty;

                            if (string.Equals(mediaType, "image"))
                            {
                                mediaUrl =
                                    j.SelectToken("images")
                                        .SelectToken("standard_resolution")
                                        .SelectToken("url")
                                        .ToString();
                            }
                            else if (string.Equals(mediaType, "video"))
                            {
                                mediaUrl =
                                    j.SelectToken("videos")
                                        .SelectToken("standard_resolution")
                                        .SelectToken("url")
                                        .ToString();
                            }

                            var captionText = j.SelectToken("caption").SelectToken("text") != null
                                ? j.SelectToken("caption").SelectToken("text").ToString()
                                : string.Empty;

                            int result;

                            using (var conn = Database.GetConnection())
                            {
                                queryString = "SELECT COUNT(*) FROM Data WHERE MediaId = @MediaId";

                                result = conn.Query<int>(queryString, new { MediaId = mediaId }).FirstOrDefault();
                            }

                            if (result == 0)
                            {
                                using (var conn = Database.GetConnection())
                                {
                                    queryString =
                                        "INSERT INTO Data VALUES (@MediaId, @Username, @DateOfPost, @Downloaded, " +
                                        "@Created, @Printed, @Collage, @MediaType, @ProfileImageUrl, @MediaUrl, " +
                                        "@ProfileImageLocation, @mediaLocation, @CaptionTextLocation, @PolaroidLocation)";
                                    conn.Query<DataModel>(queryString, new
                                    {
                                        MediaId = mediaId,
                                        Username = username,
                                        DateOfPost = dateOfPost,
                                        Downloaded = 0,
                                        Created = 0,
                                        Printed = 0,
                                        Collage = 0,
                                        MediaType = mediaType,
                                        ProfileImageUrl = profileImageUrl,
                                        MediaUrl = mediaUrl,
                                        ProfileImageLocation = string.Empty,
                                        MediaLocation = string.Empty,
                                        CaptionTextLocation = string.Empty,
                                        PolaroidLocation = string.Empty
                                    });
                                }

                                _log.Info("\t[Username:] " + username);
                                _log.Info("\t[Profile Picture:] " + profileImageUrl);
                                _log.Info("\t[Media URL:] " + mediaUrl);
                                _log.Info("\t[Media ID:] " + mediaId);
                                _log.Info("\t[Date Of Post:] " + dateOfPost);
                                _log.Info("\t[Caption:] " + captionText);

                                if (!string.IsNullOrEmpty(_hashTag2))
                                {
                                    var tags = j.SelectToken("tags").ToArray();

                                    _log.Info("Looking for second Hashtag...");

                                    if (tags.Contains(_hashTag2))
                                    {
                                        if (_download)
                                        {
                                            DownloadMedia(username, profileImageUrl, mediaUrl, mediaId, mediaType, captionText);
                                        }
                                        else
                                        {
                                            var mediaName = string.Format("{0}_{1}", username, mediaId);
                                            _log.Info("Add {0} to ignore list", mediaName);
                                        }

                                        Mediator.NotifyColleagues("InstagramCount", 1);
                                    }
                                    else
                                    {
                                        _log.Info("Does not contain second Hashtag...");

                                        _log.Trace("\t[Media ID:] " + mediaId);
                                        _log.Trace(j.SelectToken("tags").ToString());
                                    }
                                }
                                else
                                {
                                    if (_download)
                                    {
                                        _log.Info("Downloading media...");
                                        DownloadMedia(username, profileImageUrl, mediaUrl, mediaId, mediaType, captionText);
                                    }
                                    else
                                    {
                                        var mediaName = string.Format("{0}_{1}", username, mediaId);
                                        _log.Info("Add {0} to ignore list", mediaName);
                                    }

                                    Mediator.NotifyColleagues("InstagramCount", 1);
                                }
                            }
                        }
                    }
                    else
                    {
                        //_log.Trace("\t[Start Date:] " + _startDate);
                        //_log.Trace("\t[End Date:] " + _endDate);
                        //_log.Trace("\t[Username:] " + username);
                        //_log.Trace("\t[Profile Picture:] " + profileImageUrl);
                        //_log.Trace("\t[Media URL:] " + mediaUrl);
                        //_log.Trace("\t[Media ID:] " + mediaId);
                        _log.Trace("\t[Date Of Post:] " + dateOfPost);
                    }
                }
            }
        }

        private void DownloadMedia(string username, string profileImageUrl, string mediaUrl, string mediaId, string mediaType, string captionText)
        {
            if (string.Equals(mediaType.ToUpper(), "IMAGE"))
            {
                Directory.CreateDirectory(string.Format("{0}\\{1}\\Images", _folderPath, username));

                try
                {
                    _log.Info("Sending web request...");
                    using (var imageResponse = WebRequest.Create(mediaUrl).GetResponse().GetResponseStream())
                    {
                        if (imageResponse != null)
                        {
                            imageResponse.ReadTimeout = _webRequestTimeout;
                            imageResponse.WriteTimeout = _webRequestTimeout;

                            var mediaLocation = string.Format("{0}\\{1}\\Images\\{2}.jpg", _folderPath, username, mediaId);

                            using (var imageWriter = new StreamWriter(mediaLocation))
                            {
                                imageResponse.CopyTo(imageWriter.BaseStream);
                                imageResponse.Flush();

                                _log.Info("Image saved " + mediaId + ".jpg");

                                _log.Info("Downloading profile image...");
                                var profileImageLocation = string.Format("{0}\\{1}\\{1}.jpg", _folderPath, username);

                                using (
                                    var profileImageResponse =
                                        WebRequest.Create(profileImageUrl).GetResponse().GetResponseStream())
                                {
                                    if (profileImageResponse != null)
                                    {
                                        profileImageResponse.ReadTimeout = _webRequestTimeout;
                                        profileImageResponse.WriteTimeout = _webRequestTimeout;

                                        using (var profileImageWriter = new StreamWriter(profileImageLocation))
                                        {
                                            profileImageResponse.CopyTo(profileImageWriter.BaseStream);
                                            profileImageResponse.Flush();

                                            _log.Info("Profile image saved " + profileImageLocation + ".jpg");
                                        }
                                    }
                                }

                                _log.Info("Saving Caption...");
                                var captionTextLocation = string.Format("{0}\\{1}\\Images\\{2}.txt", _folderPath, username,
                                    mediaId);

                                using (var captionWriter = new StreamWriter(captionTextLocation))
                                {
                                    captionWriter.WriteLine(captionText);
                                    _log.Info("Caption saved " + mediaId + ".txt");
                                }

                                using (var conn = Database.GetConnection())
                                {
                                    var queryString =
                                        "UPDATE Data SET Downloaded = @Downloaded, ProfileImageLocation = @ProfileImageLocation, MediaLocation = @MediaLocation, CaptionTextLocation = @CaptionTextLocation WHERE MediaId = @MediaID";
                                    conn.Query<DataModel>(queryString, new
                                    {
                                        Downloaded = 1,
                                        ProfileImageLocation = profileImageLocation,
                                        MediaLocation = mediaLocation,
                                        CaptionTextLocation = captionTextLocation,
                                        MediaId = mediaId
                                    });
                                }
                                Mediator.NotifyColleagues("DownloadCount", 1);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex.ToString());
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            }
            else if (string.Equals(mediaType.ToUpper(), "VIDEO"))
            {
                Directory.CreateDirectory(string.Format("{0}\\{1}\\Videos", _folderPath, username));

                try
                {
                    _log.Info("Sending web request...");
                    using (var videoResponse = WebRequest.Create(mediaUrl).GetResponse().GetResponseStream())
                    {
                        if (videoResponse != null)
                        {
                            videoResponse.ReadTimeout = _webRequestTimeout;
                            videoResponse.WriteTimeout = _webRequestTimeout;

                            var mediaLocation = string.Format("{0}\\{1}\\Videos\\{2}.mp4", _folderPath, username, mediaId);
                            using (var imageWriter = new StreamWriter(mediaLocation))
                            {
                                videoResponse.CopyTo(imageWriter.BaseStream);
                                videoResponse.Flush();
                                _log.Info("Video saved " + mediaId + ".mp4");

                                _log.Info("Downloading profile image...");
                                var profileImageLocation = string.Format("{0}\\{1}\\{1}.jpg", _folderPath, username);
                                using (
                                    var profileImageResponse =
                                        WebRequest.Create(profileImageUrl).GetResponse().GetResponseStream())
                                {
                                    if (profileImageResponse != null)
                                    {
                                        profileImageResponse.ReadTimeout = _webRequestTimeout;
                                        profileImageResponse.WriteTimeout = _webRequestTimeout;

                                        using (var profileImageWriter = new StreamWriter(profileImageLocation))
                                        {
                                            profileImageResponse.CopyTo(profileImageWriter.BaseStream);
                                            profileImageResponse.Flush();
                                            _log.Info("Profile image saved " + username + ".jpg");
                                        }
                                    }
                                }

                                _log.Info("Saving Caption...");
                                var captionTextLocation = string.Format("{0}\\{1}\\Videos\\{2}.txt", _folderPath, username,
                                    mediaId);
                                using (var captionWriter = new StreamWriter(captionTextLocation))
                                {
                                    captionWriter.WriteLine(captionText);
                                    _log.Info("Caption saved " + mediaId + ".txt");
                                }

                                using (var conn = Database.GetConnection())
                                {
                                    var queryString =
                                        "UPDATE Data SET Downloaded = @Downloaded, ProfileImageLocation = @ProfileImageLocation, MediaLocation = @MediaLocation, CaptionTextLocation = @CaptionTextLocation WHERE MediaId = @MediaID";
                                    conn.Query<DataModel>(queryString, new
                                    {
                                        Downloaded = 1,
                                        ProfileImageLocation = profileImageLocation,
                                        MediaLocation = mediaLocation,
                                        CaptionTextLocation = captionTextLocation,
                                        MediaId = mediaId
                                    });
                                }
                                Mediator.NotifyColleagues("DownloadCount", 1);
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(ex.ToString());
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                }
            }
        }

        public void Stop()
        {
            IsRunning = false;
            _log.Info("Stopping Instagram service...");
        }

        public string NextPageUrl { get; set; }

        public bool IsRunning { get; private set; }
    }
}