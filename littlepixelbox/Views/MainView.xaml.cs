﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Designer.ColorFontPicker;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace littlepixelbox.Views
{
    public partial class MainWindow
    {
        private Control _selectedControl;

        public MainWindow()
        {
            InitializeComponent();

            // Set defaut colorpickers when window is initiated
            ColorPickerBackground.SelectedColor = Colors.White;
            ColorPickerLocationIcon.SelectedColor = Color.FromRgb(22, 87, 134);
            ColorPickerCaptionIcon.SelectedColor = Color.FromRgb(22, 87, 134);

            _selectedControl = null;
        }

        /// <summary>
        ///     Enable/Disable Move & Resize Adorners
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowAdornerDesignerItems(object sender, RoutedEventArgs e)
        {
            if (ToggleDesigner.IsChecked == true)
            {
                foreach (var child in Utils.FindVisualChildren<Control>(PolaroidCanvas))
                {
                    Selector.SetIsSelected(child, child.Visibility == Visibility.Visible);
                }
            }
            else
            {
                foreach (var child in Utils.FindVisualChildren<Control>(PolaroidCanvas))
                {
                    Selector.SetIsSelected(child, false);
                }
            }
        }

        private void control_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Selector.SetIsSelected((Control) sender, true);
        }

        private void control_PreviewMouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (ToggleDesigner.IsChecked != null && !(bool) ToggleDesigner.IsChecked)
            {
                Selector.SetIsSelected((Control) sender, false);
            }

            _selectedControl = (Control) sender;
            _selectedControl.Focus();
        }

        private void polaroidCanvas_KeyDown(object sender, KeyEventArgs e)
        {
            var canvasControls = Utils.FindVisualChildren<Control>(PolaroidCanvas).Select(child => child.Name).ToList();

            if (_selectedControl != null && canvasControls.Contains(_selectedControl.Name))
            {
                var dragDeltaHorizontal = 0;
                var dragDeltaVertical = 0;

                switch (e.Key)
                {
                    case Key.Left:
                        dragDeltaHorizontal = -1;
                        break;
                    case Key.Right:
                        dragDeltaHorizontal = 1;
                        break;
                    case Key.Up:
                        dragDeltaVertical = -1;
                        break;
                    case Key.Down:
                        dragDeltaVertical = 1;
                        break;
                }

                var left = (double) _selectedControl.GetValue(Canvas.LeftProperty);
                var top = (double) _selectedControl.GetValue(Canvas.TopProperty);

                var maxRight = PolaroidCanvas.Width - _selectedControl.Width;
                var maxBottom = PolaroidCanvas.Height - _selectedControl.Height;

                dragDeltaHorizontal = left + dragDeltaHorizontal >= 0 && left + dragDeltaHorizontal <= maxRight
                    ? dragDeltaHorizontal
                    : 0;
                dragDeltaVertical = top + dragDeltaVertical >= 0 && top + dragDeltaVertical <= maxBottom
                    ? dragDeltaVertical
                    : 0;

                _selectedControl.SetValue(Canvas.LeftProperty, left + dragDeltaHorizontal);
                _selectedControl.SetValue(Canvas.TopProperty, top + dragDeltaVertical);

                e.Handled = true;
            }
        }

        #region Checkboxes

        private void chkBoxBackgroundColor_Checked(object sender, RoutedEventArgs e)
        {
            ChkBoxBackgroundImage.IsChecked = false;
        }

        private void chkBoxBackgroundColor_Unchecked(object sender, RoutedEventArgs e)
        {
            ColorPickerBackground.SelectedColor = Colors.White;
        }

        private void chkBoxBackgroundImage_Checked(object sender, RoutedEventArgs e)
        {
            ChkBoxBackgroundColor.IsChecked = false;
        }

        #endregion

        #region Buttons

        private void btnUsernameFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockUsername)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockUsername, selectedFont);
                }
            }
        }

        private void btnCaptionFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockCaption)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockCaption, selectedFont);
                }
            }
        }

        private void btnTimeFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockTime)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockTime, selectedFont);
                }
            }
        }

        private void btnLocationFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockLocation)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockLocation, selectedFont);
                }
            }
        }

        private void btnBrandingFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockBranding)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockBranding, selectedFont);
                }
            }
        }

        private void btnCustomTextFont_Click(object sender, RoutedEventArgs e)
        {
            var fntDialog = new ColorFontDialog
            {
                Owner = this,
                Font = FontInfo.GetTextBlockFont(TxtBlockCustomText)
            };

            if (fntDialog.ShowDialog() == true)
            {
                var selectedFont = fntDialog.Font;

                if (selectedFont != null)
                {
                    FontInfo.ApplyTextBlockFont(TxtBlockCustomText, selectedFont);
                }
            }
        }

        private void btnLogo_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };

            dialog.Filters.Add(new CommonFileDialogFilter("Images", ".png, .jpg, .bmp, .gif"));

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                BitmapSource bitmap = Utils.LoadImageUri(new Uri(dialog.FileName, UriKind.Absolute));
                Imglogo.Source = bitmap;
            }
        }

        private void btnBackgroundImage_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };

            dialog.Filters.Add(new CommonFileDialogFilter("Images", ".png, .jpg, .bmp, .gif"));

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                ImgPolaroidBackgroundImage.Source = new BitmapImage(new Uri(dialog.FileName, UriKind.Absolute));
            }
        }

        private void btnOverlay_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };

            dialog.Filters.Add(new CommonFileDialogFilter("Images", ".png, .jpg, .bmp, .gif"));

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                ImgPolaroidOverlay.Source = new BitmapImage(new Uri(dialog.FileName, UriKind.Absolute));
            }
        }

        private void btnLoadTemplate_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };
            dialog.Filters.Add(new CommonFileDialogFilter("Template", ".template"));

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Mediator.NotifyColleagues("LoadTemplate", dialog.FileName);

                // Sync colorpicker's
                var a = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).A;
                var r = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).R;
                var g = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).G;
                var b = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).B;
                ColorPickerBackground.SelectedColor = Color.FromArgb(a, r, g, b);

                a = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).A;
                r = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).R;
                g = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).G;
                b = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).B;
                ColorPickerCaptionIcon.SelectedColor = Color.FromArgb(a, r, g, b);

                a = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).A;
                r = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).R;
                g = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).G;
                b = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).B;
                ColorPickerLocationIcon.SelectedColor = Color.FromArgb(a, r, g, b);
            }
        }

        private void btnReset_Click(object sender, RoutedEventArgs e)
        {
            Mediator.NotifyColleagues("ResetTemplate", true);

            // Sync colorpicker's
            var a = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).A;
            var r = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).R;
            var g = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).G;
            var b = ((Color) PolaroidCanvas.Background.GetValue(SolidColorBrush.ColorProperty)).B;
            ColorPickerBackground.SelectedColor = Color.FromArgb(a, r, g, b);

            a = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).A;
            r = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).R;
            g = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).G;
            b = ((Color) CcCaptionIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).B;
            ColorPickerCaptionIcon.SelectedColor = Color.FromArgb(a, r, g, b);

            a = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).A;
            r = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).R;
            g = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).G;
            b = ((Color) CcLocationIcon.Foreground.GetValue(SolidColorBrush.ColorProperty)).B;
            ColorPickerLocationIcon.SelectedColor = Color.FromArgb(a, r, g, b);
        }

        private void btnSaveTemplate_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonSaveFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop)
            };
            dialog.Filters.Add(new CommonFileDialogFilter("Template", ".template"));
            dialog.DefaultExtension = "template";
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                Mediator.NotifyColleagues("SaveTemplate", dialog.FileName);
            }
        }

        #endregion

        #region Color Pickers

        private void colorPickerBackground_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {
            PolaroidCanvas.Background = new SolidColorBrush(ColorPickerBackground.SelectedColor);
        }

        private void colorPickerLocationIcon_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {
            ImgLocationIcon.Source = Utils.ModifyBitmapColors((BitmapImage) ImgLocationIcon.Source,
                ColorPickerLocationIcon.SelectedColor.R,
                ColorPickerLocationIcon.SelectedColor.G, ColorPickerLocationIcon.SelectedColor.B);

            CcLocationIcon.Foreground = new SolidColorBrush(ColorPickerLocationIcon.SelectedColor);
        }

        private void colorPickerCaptionIcon_SelectedColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {
            ImgCaptionIcon.Source = Utils.ModifyBitmapColors((BitmapImage) ImgCaptionIcon.Source,
                ColorPickerCaptionIcon.SelectedColor.R,
                ColorPickerCaptionIcon.SelectedColor.G, ColorPickerCaptionIcon.SelectedColor.B);

            CcCaptionIcon.Foreground = new SolidColorBrush(ColorPickerCaptionIcon.SelectedColor);
        }

        #endregion
    }
}