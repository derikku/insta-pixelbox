﻿using System.Windows;
using Xceed.Wpf.Toolkit;

namespace littlepixelbox.Views
{
    public partial class MainTabView
    {
        public MainTabView()
        {
            InitializeComponent();
        }

        private void intUpDownHours_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
            {
                var integerUpDown = sender as IntegerUpDown;
                if (integerUpDown != null) integerUpDown.Value = 0;
            }
        }

        private void intUpDownMinutes_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
            {
                var integerUpDown = sender as IntegerUpDown;
                if (integerUpDown != null) integerUpDown.Value = 0;
            }
        }

        private void intUpDownSeconds_ValueChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            if (e.NewValue == null)
            {
                var integerUpDown = sender as IntegerUpDown;
                if (integerUpDown != null) integerUpDown.Value = 0;
            }
        }
    }
}