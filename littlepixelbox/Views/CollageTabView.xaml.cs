﻿using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace littlepixelbox.Views
{
    public partial class CollageTabView
    {
        public CollageTabView()
        {
            InitializeComponent();
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void Hyperlink_RequestNavigate(object sender, RequestNavigateEventArgs e)
        {
            Process.Start(new ProcessStartInfo(e.Uri.AbsoluteUri));
            e.Handled = true;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void sliderScrollSpeed_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            SetCollagePreviewImg(SliderImgRow.Value);
        }

        private void SetCollagePreviewImg(double imgPerRow)
        {
            if (SpCollagePreview != null)
            {
                SpCollagePreview.Children.Clear();

                var size = SpCollagePreview.Width/imgPerRow;

                for (var i = 0; i < imgPerRow; i++)
                {
                    var img = new Image
                    {
                        Height = size,
                        Width = size,
                        Source =
                            new BitmapImage(new Uri(string.Format("/Resources/Collage/{0}.jpg", i), UriKind.Relative))
                    };

                    SpCollagePreview.Children.Add(img);
                }

                var duration = TimeSpan.FromSeconds((CanvasCollagePreview.Height + size)/SliderScrollSpeed.Value);

                AnimationCollagePreview.To = -size;
                AnimationCollagePreview.Duration = duration;

                // Restart Storyboard
                SbCollagePreview.Stop();
                SbCollagePreview.Begin();
            }
        }
    }
}