﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using Microsoft.WindowsAPICodePack.Dialogs;

namespace littlepixelbox.Views
{
    public partial class ToolsTabView
    {
        public ToolsTabView()
        {
            InitializeComponent();
        }

        private void btnRootFolder_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", AppDomain.CurrentDomain.BaseDirectory);
        }

        private void btnLogFolder_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", "Logs");
        }

        private void btnExport_Click(object sender, RoutedEventArgs e)
        {
            var dialog = new CommonOpenFileDialog
            {
                InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                IsFolderPicker = true
            };

            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                TxtBoxExport.Text = dialog.FileName;
            }
        }

        private void btnOpenExportFolder_Click(object sender, RoutedEventArgs e)
        {
            Process.Start("explorer.exe", TxtBoxExport.Text);
        }

        private void btnDeleteAll_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                if (Directory.Exists("Instagram"))
                {
                    var di = new DirectoryInfo(@"Instagram");

                    foreach (var d in di.EnumerateDirectories())
                    {
                        d.Delete(true);
                    }
                }

                if (Directory.Exists("Logs"))
                    Directory.Delete("Logs", true);

                if (Directory.Exists(TxtBoxExport.Text))
                {
                    var diExport = new DirectoryInfo(TxtBoxExport.Text);
                    foreach (var file in diExport.GetFiles())
                    {
                        file.Delete();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnHttpConfig_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Tools\HttpConfig.exe"));
        }

        private void btnCompactView_Click(object sender, RoutedEventArgs e)
        {
            Process.Start(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"Tools\CompactView.exe"));
        }
    }
}