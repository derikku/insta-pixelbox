﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows;
using System.Windows.Navigation;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using Microsoft.Win32;
using NLog;

namespace littlepixelbox.Views
{
    /// <summary>
    /// Interaction logic for InstagramLogin.xaml
    /// </summary>
    public partial class InstagramLogin
    {
        [DllImport("wininet.dll", CharSet = CharSet.Auto, SetLastError = true)]

        private static extern bool InternetSetOption(IntPtr hInternet, int dwOption, IntPtr lpBuffer, int dwBufferLength);

        private const int InternetOptionSuppressBehavior = 81;
        private const int InternetSuppressCookiePersist = 3;

        private readonly Logger _log = LogManager.GetLogger("LittlePixelBox");

        public InstagramLogin()
        {
            SetBrowserFeatureControl();

            InitializeComponent();

            SuppressCookiePersistence();

            var url = string.Format(@"https://api.instagram.com/oauth/authorize/?client_id={0}&redirect_uri={1}&response_type=token&scope=public_content", 
                AppSettings.Get<string>("InstagramClientID"), AppSettings.Get<string>("InstagramReturnUri"));

            Webb.Visibility = Visibility.Visible;
            Webb.LoadCompleted += Webb_LoadCompleted;
            Webb.Navigate(url);
        }

        /// <summary>
        /// Suppress WebBrowser script errors
        /// </summary>
        public static void SuppressCookiePersistence()
        {
            var lpBuffer = Marshal.AllocCoTaskMem(Marshal.SizeOf(typeof(int)));
            Marshal.StructureToPtr(InternetSuppressCookiePersist, lpBuffer, true);

            InternetSetOption(IntPtr.Zero, InternetOptionSuppressBehavior, lpBuffer, sizeof(int));

            Marshal.FreeCoTaskMem(lpBuffer);
        }

        private void Webb_LoadCompleted(object sender, NavigationEventArgs e)
        {
            Webb.LoadCompleted -= Webb_LoadCompleted; //REMOVE THE OLD EVENT METHOD BINDING
            Webb.Navigated += Webb_Navigated;
            Webb.LoadCompleted += Webb_LoadCompleted2; //BIND TO A NEW METHOD FOR THE EVENT
        }

        private void Webb_Navigated(object sender, NavigationEventArgs e)
        {
            // Close window
            Close();
        }

        private void Webb_LoadCompleted2(object sender, NavigationEventArgs e)
        {
            var url = Webb.Source.ToString();

            if (url.Contains("access_token"))
            {
                var accessToken = url.Substring(url.IndexOf("=", StringComparison.Ordinal) + 1);
                InstagramAccessTokenSingleton.Instance.AccessToken = accessToken;
            }
            else
            {
                mshtml.HTMLDocument dom = (mshtml.HTMLDocument)Webb.Document;

                var httpError = new[] {"401", "402", "403"}.Any(c => dom.title.Contains(c));

                if (httpError)
                {
                    _log.Error("Error logging into Instagram: {0}", dom.title);
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", dom.title);
                }
                else
                {
                    Process.Start(url);
                }
            }
        }

        public static bool ContainsAny(string str, params string[] values)
        {
            return values.Any(str.Contains);
        }

        private void SetBrowserFeatureControlKey(string feature, string appName, uint value)
        {
            using (var key = Registry.CurrentUser.CreateSubKey(
                String.Concat(@"Software\Microsoft\Internet Explorer\Main\FeatureControl\", feature),
                RegistryKeyPermissionCheck.ReadWriteSubTree))
            {
                if (key != null) key.SetValue(appName, value, RegistryValueKind.DWord);
            }
        }

        /// <summary>
        /// Set WebBrowser to use the latest version of IE
        /// </summary>
        private void SetBrowserFeatureControl()
        {
            // FeatureControl settings are per-process
            var fileName = System.IO.Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);

            // make the control is not running inside Visual Studio Designer
            if (string.Compare(fileName, "devenv.exe", StringComparison.OrdinalIgnoreCase) == 0 || string.Compare(fileName, "XDesProc.exe", StringComparison.OrdinalIgnoreCase) == 0)
                return;

            SetBrowserFeatureControlKey("FEATURE_BROWSER_EMULATION", fileName, GetBrowserEmulationMode()); // Webpages containing standards-based !DOCTYPE directives are displayed in IE10 Standards mode.
            SetBrowserFeatureControlKey("FEATURE_AJAX_CONNECTIONEVENTS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ENABLE_CLIPCHILDREN_OPTIMIZATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_MANAGE_SCRIPT_CIRCULAR_REFS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_DOMSTORAGE ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_GPU_RENDERING ", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_IVIEWOBJECTDRAW_DMLT9_WITH_GDI  ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_LEGACY_COMPRESSION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_LOCALMACHINE_LOCKDOWN", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_OBJECT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_BLOCK_LMZ_SCRIPT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_DISABLE_NAVIGATION_SOUNDS", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SCRIPTURL_MITIGATION", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_SPELLCHECKING", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_STATUS_BAR_THROTTLING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_TABBED_BROWSING", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_VALIDATE_NAVIGATE_URL", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_DOCUMENT_ZOOM", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_POPUPMANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBOC_MOVESIZECHILD", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_ADDON_MANAGEMENT", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_WEBSOCKET", fileName, 1);
            SetBrowserFeatureControlKey("FEATURE_WINDOW_RESTRICTIONS ", fileName, 0);
            SetBrowserFeatureControlKey("FEATURE_XMLHTTP", fileName, 1);
        }

        private static uint GetBrowserEmulationMode()
        {
            int browserVersion = 0;

            using (var ieKey = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Microsoft\Internet Explorer",
                RegistryKeyPermissionCheck.ReadSubTree,
                System.Security.AccessControl.RegistryRights.QueryValues))
            {
                if (ieKey != null)
                {
                    var version = ieKey.GetValue("svcVersion");
                    if (null == version)
                    {
                        version = ieKey.GetValue("Version");
                        if (null == version)
                            throw new ApplicationException("Microsoft Internet Explorer is required!");
                    }
                    int.TryParse(version.ToString().Split('.')[0], out browserVersion);
                }
            }

            var mode = 11000; // Internet Explorer 11. Webpages containing standards-based !DOCTYPE directives are displayed in IE11 Standards mode. Default value for Internet Explorer 11.
            switch (browserVersion)
            {
                case 7:
                    mode = 7000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE7 Standards mode. Default value for applications hosting the WebBrowser Control.
                    break;
                case 8:
                    mode = 8000; // Webpages containing standards-based !DOCTYPE directives are displayed in IE8 mode. Default value for Internet Explorer 8
                    break;
                case 9:
                    mode = 9000; // Internet Explorer 9. Webpages containing standards-based !DOCTYPE directives are displayed in IE9 mode. Default value for Internet Explorer 9.
                    break;
                case 10:
                    mode = 10000; // Internet Explorer 10. Webpages containing standards-based !DOCTYPE directives are displayed in IE10 mode. Default value for Internet Explorer 10.
                    break;
            }

            return (uint) mode;
        }
    }
}
