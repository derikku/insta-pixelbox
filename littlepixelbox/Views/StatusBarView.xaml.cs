﻿using System.Windows.Input;

namespace littlepixelbox.Views
{
    public partial class StatusBarView
    {
        public StatusBarView()
        {
            InitializeComponent();
        }

        private void StatusBarItem_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            txtBlockErrorMessage.Text = string.Empty;
        }
    }
}
