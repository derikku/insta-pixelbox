﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;

namespace littlepixelbox.ViewModels
{
    internal class MainViewModel : ViewModelBase
    {
        private readonly BackgroundWorker _bgWrkCreatePolaroid = new BackgroundWorker();
        private bool _enableGui = true;

        private PolaroidService _polaroidSerivce;
        private bool _showAdornerDesigner;
        private bool _showGridLines;

        private readonly DispatcherTimer _timerCreatePolaroid = new DispatcherTimer();
        private readonly Logger _log = LogManager.GetLogger("LittlePixelBox");
        private readonly Logger _logPolaroid = LogManager.GetLogger("PolaroidService");

        public MainViewModel()
        {
            Mediator.Register("RunCreatePolaroidService", RunCreatePolaroidService);
            Mediator.Register("SetPolaroidSingleton", SetPolaroidSingleton);
            Mediator.Register("SaveTemplate", SaveTemplate);
            Mediator.Register("LoadTemplate", LoadTemplate);
            Mediator.Register("ResetTemplate", SetDeaultLayout);
            Mediator.Register("SetLayoutSources", SetLayoutSources);

            SetDeaultLayout(true);

            // Hook up the timer
            _timerCreatePolaroid.Tick += timer_Tick;
            _timerCreatePolaroid.Interval = TimeSpan.FromSeconds(1);

            // Hook up background worker
            _bgWrkCreatePolaroid.DoWork += bgWrkCreatePolaroid_DoWork;
        }

        private void SetDeaultLayout(object args)
        {
            PolaroidWidth = 300;
            PolaroidHeight = 450;
            PolaroidDpi = AppSettings.Get<int>("PolaroidDpi");
            PolaroidBackgroundColor = Brushes.White;
            PolaroidBackgroundImage = new BitmapImage(new Uri("../Resources/background.png", UriKind.Relative));
            PolaroidOverlay = new BitmapImage(new Uri("../Resources/overlay.png", UriKind.Relative));
            PolaroidBackgroundImageVisible = false;
            PolaroidOverlayVisible = false;

            Username = "Username";
            UsernameWidth = 229;
            UsernameHeight = 17;
            UsernameLeft = 59;
            UsernameTop = 13;
            UsernameFontSize = 12;
            UsernameFontFamily = new FontFamily("Segoe UI");
            UsernameFontStyle = FontStyles.Normal;
            UsernameFontStretch = FontStretches.Normal;
            UsernameFontWeight = FontWeights.Bold;
            UsernameForeground = (SolidColorBrush)new BrushConverter().ConvertFrom("#165786");
            UsernameVisible = true;

            Location = AppSettings.Get<string>("DefaultLocation");
            LocationWidth = 218;
            LocationHeight = 15;
            LocationLeft = 70;
            LocationTop = 31;
            LocationFontSize = 11;
            LocationFontFamily = new FontFamily("Segoe UI");
            LocationFontStyle = FontStyles.Normal;
            LocationFontStretch = FontStretches.Normal;
            LocationFontWeight = FontWeights.Bold;
            LocationForeground = (SolidColorBrush) new BrushConverter().ConvertFrom("#FF696969");
            LocationVisible = true;

            Caption = "Having lots of fun \U0001F389 with @littlepixelbox #fun #hashtag \U0001F604";
            CaptionWidth = 263;
            CaptionHeight = 30;
            CaptionLeft = 27;
            CaptionTop = 335;
            CaptionFontSize = 11;
            CaptionFontFamily = new FontFamily("Segoe UI Emoji");
            CaptionFontStyle = FontStyles.Normal;
            CaptionFontStretch = FontStretches.Normal;
            CaptionFontWeight = FontWeights.Normal;
            CaptionForeground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FF696969");
            CaptionVisible = true;

            Branding = "wwww.littlepixelbox.com.au";
            BrandingWidth = 76;
            BrandingHeight = 8;
            BrandingLeft = 114;
            BrandingTop = 440;
            BrandingFontSize = 6;
            BrandingFontFamily = new FontFamily("Segoe UI");
            BrandingFontStyle = FontStyles.Normal;
            BrandingFontStretch = FontStretches.Normal;
            BrandingFontWeight = FontWeights.Normal;
            BrandingForeground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FF696969");
            BrandingVisible = true;

            CustomText = "";
            CustomTextWidth = 76;
            CustomTextHeight = 15;
            CustomTextLeft = 214;
            CustomTextTop = 13;
            CustomTextFontSize = 11;
            CustomTextFontFamily = new FontFamily("Segoe UI");
            CustomTextFontStyle = FontStyles.Normal;
            CustomTextFontStretch = FontStretches.Normal;
            CustomTextFontWeight = FontWeights.Normal;
            CustomTextForeground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FF696969");
            CustomTextVisible = false;

            TimeWidth = 50;
            TimeHeight = 15;
            TimeLeft = 240;
            TimeTop = 13;
            TimeFontSize = 11;
            TimeFontFamily = new FontFamily("Segoe UI");
            TimeFontStyle = FontStyles.Normal;
            TimeFontStretch = FontStretches.Normal;
            TimeFontWeight = FontWeights.Bold;
            TimeForeground = (SolidColorBrush)new BrushConverter().ConvertFrom("#FF696969");
            TimeVisible = false;

            ProfileImageSource = new BitmapImage(new Uri("../Resources/profile_image.png", UriKind.Relative));
            ProfileImageWidth = 40;
            ProfileImageHeight = 40;
            ProfileImageLeft = 10;
            ProfileImageTop = 10;
            ProfileImageVisible = true;

            ImageSource = new BitmapImage(new Uri("../Resources/instagram_image.png", UriKind.Relative));
            ImageWidth = 280;
            ImageHeight = 280;
            ImageLeft = 10;
            ImageTop = 55;
            ImageVisible = true;

            LocationIconSource = new BitmapImage(new Uri("../Resources/location.png", UriKind.Relative));
            LocationIconColor = new SolidColorBrush(Color.FromRgb(22, 87, 134));
            LocationIconWidth = 8;
            LocationIconHeight = 11;
            LocationIconLeft = 59;
            LocationIconTop = 34;
            LocationIconVisible = true;

            CaptionIconSource = new BitmapImage(new Uri("../Resources/caption.png", UriKind.Relative));
            CaptionIconColor = new SolidColorBrush(Color.FromRgb(22, 87, 134));
            CaptionIconWidth = 13;
            CaptionIconHeight = 11;
            CaptionIconLeft = 10;
            CaptionIconTop = 338;
            CaptionIconVisible = true;

            LogoSource = new BitmapImage(new Uri("../Resources/Logo.png", UriKind.Relative));
            LogoWidth = 189;
            LogoHeight = 60;
            LogoLeft = 59;
            LogoTop = 375;
            LogoVisible = true;
        }

        private void SetLayoutSources(object args)
        {
            try
            {
                var values = (Dictionary<string, string>) args;

                Username = values["username"];
                Caption = values["captionText"];
                var bitmap2 = new BitmapImage();
                bitmap2.BeginInit();
                bitmap2.UriSource = new Uri(values["profileImageUrl"], UriKind.Absolute);
                bitmap2.EndInit();
                ProfileImageSource = bitmap2;

                var bitmap = new BitmapImage();
                bitmap.BeginInit();
                bitmap.UriSource = new Uri(values["mediaUrl"], UriKind.Absolute);
                bitmap.EndInit();
                ImageSource = bitmap;
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        public void RunCreatePolaroidService(object args)
        {
            if ((bool) args)
            {
                // Freeze Freezable Objects to allow to be shared across threads.
                if (_polaroidBackgroundImage != null)
                {
                    _polaroidBackgroundImage.Freeze();
                }
                if (_polaroidOverlay != null)
                {
                    _polaroidOverlay.Freeze();
                }
                if (_logoSource != null)
                {
                    _logoSource.Freeze();
                }
                _polaroidBackgroundColor.Freeze();
                _usernameForeground.Freeze();
                _captionForeground.Freeze();
                _brandingForeground.Freeze();
                _customTextForeground.Freeze();
                _timeForeground.Freeze();
                _locationForeground.Freeze();
                _profileImageSource.Freeze();
                _locationIconSource.Freeze();
                _captionIconSource.Freeze();

                _timerCreatePolaroid.Start();
                _logPolaroid.Info("Polaroid Service started.");

                ShowAdornerDesigner = false;
                ShowGridLines = false;
                EnableGui = false;
            }
            else
            {
                _timerCreatePolaroid.Stop();
                _logPolaroid.Info("Polaroid Service stopped.");

                EnableGui = true;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!_bgWrkCreatePolaroid.IsBusy)
            {
                _bgWrkCreatePolaroid.RunWorkerAsync();
            }
        }

        private void bgWrkCreatePolaroid_DoWork(object sender, DoWorkEventArgs e)
        {
            _polaroidSerivce = new PolaroidService(GetPolaroidModel());
            _polaroidSerivce.CreatePolaroid();
        }

        private void SaveTemplate(object filename)
        {
            var obj = new
            {
                Polaroid = new
                {
                    Width = _polaroidWidth,
                    Height = _polaroidHeight,
                    DPI = _polaroidDpi,
                    BackgroundColor = _polaroidBackgroundColor,
                    BackgroundImage = _polaroidBackgroundImage,
                    BackgroundImageVisible = _polaroidBackgroundImageVisible,
                    Overlay = _polaroidOverlay,
                    OverlayVisible = _polaroidOverlayVisible
                },
                ProfileImage = new
                {
                    Visible = _profileImageVisible,
                    Width = _profileImageWidth,
                    Height = _profileImageHeight,
                    Left = _profileImageLeft,
                    Top = _profileImageTop
                },
                Image = new
                {
                    Visible = _imageVisible,
                    Width = _imageWidth,
                    Height = _imageHeight,
                    Left = _imageLeft,
                    Top = _imageTop
                },
                LocationIcon = new
                {
                    Visible = _locationIconVisible,
                    Color = _locationIconColor,
                    Width = _locationIconWidth,
                    Height = _locationIconHeight,
                    Left = _locationIconLeft,
                    Top = _locationIconTop
                },
                CaptionIcon = new
                {
                    Visible = _captionIconVisible,
                    Color = _captionIconColor,
                    Width = _captionIconWidth,
                    Height = _captionIconHeight,
                    Left = _captionIconLeft,
                    Top = _captionIconTop
                },
                Logo = new
                {
                    Visible = _logoVisible,
                    Source = _logoSource,
                    Width = _logoWidth,
                    Height = _logoHeight,
                    Left = _logoLeft,
                    Top = _logoTop
                },
                Username = new
                {
                    Visible = _usernameVisible,
                    FontFamily = _usernameFontFamily,
                    FontStyle = _usernameFontStyle,
                    FontWeight = _usernameFontWeight,
                    FontStretch = _usernameFontStretch,
                    FontSize = _usernameFontSize,
                    Foreground = _usernameForeground,
                    Width = _usernameWidth,
                    Height = _usernameHeight,
                    Left = _usernameLeft,
                    Top = _usernameTop
                },
                Location = new
                {
                    Visible = _locationVisible,
                    Text = _location,
                    FontFamily = _locationFontFamily,
                    FontStyle = _locationFontStyle,
                    FontWeight = _locationFontWeight,
                    FontStretch = _locationFontStretch,
                    FontSize = _locationFontSize,
                    Foreground = _locationForeground,
                    Width = _locationWidth,
                    Height = _locationHeight,
                    Left = _locationLeft,
                    Top = _locationTop
                },
                Caption = new
                {
                    Visible = _captionVisible,
                    FontFamily = _captionFontFamily,
                    FontStyle = _captionFontStyle,
                    FontWeight = _captionFontWeight,
                    FontStretch = _captionFontStretch,
                    FontSize = _captionFontSize,
                    Foreground = _captionForeground,
                    Width = _captionWidth,
                    Height = _captionHeight,
                    Left = _captionLeft,
                    Top = _captionTop
                },
                Branding = new
                {
                    Visible = _brandingVisible,
                    Text = _branding,
                    FontFamily = _brandingFontFamily,
                    FontStyle = _brandingFontStyle,
                    FontWeight = _brandingFontWeight,
                    FontStretch = _brandingFontStretch,
                    FontSize = _brandingFontSize,
                    Foreground = _brandingForeground,
                    Width = _brandingWidth,
                    Height = _brandingHeight,
                    Left = _brandingLeft,
                    Top = _brandingTop
                },
                CustomText = new
                {
                    Visible = _customTextVisible,
                    Text = _customText,
                    FontFamily = _customTextFontFamily,
                    FontStyle = _customTextFontStyle,
                    FontWeight = _customTextFontWeight,
                    FontStretch = _brandingFontStretch,
                    FontSize = _customTextFontSize,
                    Foreground = _customTextForeground,
                    Width = _customTextWidth,
                    Height = _customTextHeight,
                    Left = _customTextLeft,
                    Top = _customTextTop
                },
                Time = new
                {
                    Visible = _timeVisible,
                    FontFamily = _timeFontFamily,
                    FontStyle = _timeFontStyle,
                    FontWeight = _timeFontWeight,
                    FontStretch = _brandingFontStretch,
                    FontSize = _timeFontSize,
                    Foreground = _timeForeground,
                    Width = _timeWidth,
                    Height = _timeHeight,
                    Left = _timeLeft,
                    Top = _timeTop
                }
            };

            try
            {
                var template = JsonConvert.SerializeObject(obj, Formatting.Indented);
                File.WriteAllText(filename.ToString(), template);
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void LoadTemplate(object filename)
        {
            try
            {
                var template = File.ReadAllText(filename.ToString());

                var token = JObject.Parse(template);

                PolaroidWidth = (int) token.SelectToken("Polaroid").SelectToken("Width");
                PolaroidHeight = (int) token.SelectToken("Polaroid").SelectToken("Height");
                PolaroidDpi = (int) token.SelectToken("Polaroid").SelectToken("DPI");
                PolaroidBackgroundColor =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("Polaroid").SelectToken("BackgroundColor").ToString());
                PolaroidBackgroundImage =
                    new BitmapImage(new Uri(token.SelectToken("Polaroid").SelectToken("BackgroundImage").ToString(),
                        UriKind.RelativeOrAbsolute));
                PolaroidBackgroundImageVisible =
                    (bool) token.SelectToken("Polaroid").SelectToken("BackgroundImageVisible");
                PolaroidOverlay =
                    new BitmapImage(new Uri(token.SelectToken("Polaroid").SelectToken("Overlay").ToString(),
                        UriKind.RelativeOrAbsolute));
                PolaroidOverlayVisible = (bool) token.SelectToken("Polaroid").SelectToken("OverlayVisible");

                ProfileImageVisible = (bool) token.SelectToken("ProfileImage").SelectToken("Visible");
                ProfileImageWidth = (int) token.SelectToken("ProfileImage").SelectToken("Width");
                ProfileImageHeight = (int) token.SelectToken("ProfileImage").SelectToken("Height");
                ProfileImageLeft = (int) token.SelectToken("ProfileImage").SelectToken("Left");
                ProfileImageTop = (int) token.SelectToken("ProfileImage").SelectToken("Top");

                ImageVisible = (bool) token.SelectToken("Image").SelectToken("Visible");
                ImageWidth = (int) token.SelectToken("Image").SelectToken("Width");
                ImageHeight = (int) token.SelectToken("Image").SelectToken("Height");
                ImageLeft = (int) token.SelectToken("Image").SelectToken("Left");
                ImageTop = (int) token.SelectToken("Image").SelectToken("Top");

                LocationIconVisible = (bool) token.SelectToken("LocationIcon").SelectToken("Visible");
                LocationIconColor =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("LocationIcon").SelectToken("Color").ToString());
                LocationIconWidth = (int) token.SelectToken("LocationIcon").SelectToken("Width");
                LocationIconHeight = (int) token.SelectToken("LocationIcon").SelectToken("Height");
                LocationIconLeft = (int) token.SelectToken("LocationIcon").SelectToken("Left");
                LocationIconTop = (int) token.SelectToken("LocationIcon").SelectToken("Top");

                CaptionIconVisible = (bool) token.SelectToken("CaptionIcon").SelectToken("Visible");
                CaptionIconColor =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("CaptionIcon").SelectToken("Color").ToString());
                CaptionIconWidth = (int) token.SelectToken("CaptionIcon").SelectToken("Width");
                CaptionIconHeight = (int) token.SelectToken("CaptionIcon").SelectToken("Height");
                CaptionIconLeft = (int) token.SelectToken("CaptionIcon").SelectToken("Left");
                CaptionIconTop = (int) token.SelectToken("CaptionIcon").SelectToken("Top");

                LogoVisible = (bool) token.SelectToken("Logo").SelectToken("Visible");
                LogoSource =
                    new BitmapImage(new Uri(token.SelectToken("Logo").SelectToken("Source").ToString(),
                        UriKind.RelativeOrAbsolute));
                LogoWidth = (int) token.SelectToken("Logo").SelectToken("Width");
                LogoHeight = (int) token.SelectToken("Logo").SelectToken("Height");
                LogoLeft = (int) token.SelectToken("Logo").SelectToken("Left");
                LogoTop = (int) token.SelectToken("Logo").SelectToken("Top");

                UsernameVisible = (bool) token.SelectToken("Username").SelectToken("Visible");
                UsernameFontFamily = new FontFamily(token.SelectToken("Username").SelectToken("FontFamily").ToString());
                var cfs = new FontStyleConverter().ConvertFromString(token.SelectToken("Username").SelectToken("FontStyle").ToString());
                if (cfs != null)
                    UsernameFontStyle = (FontStyle) cfs;
                cfs = new FontStretchConverter().ConvertFromString(token.SelectToken("Username").SelectToken("FontStretch").ToString());
                if (cfs != null)
                    UsernameFontStretch = (FontStretch) cfs;
                cfs = new FontWeightConverter().ConvertFromString(token.SelectToken("Username").SelectToken("FontWeight").ToString());
                if (cfs != null) UsernameFontWeight = (FontWeight) cfs;
                UsernameFontSize = (int) token.SelectToken("Username").SelectToken("FontSize");
                UsernameForeground = (SolidColorBrush) new BrushConverter().ConvertFrom( token.SelectToken("Username").SelectToken("Foreground").ToString());
                UsernameWidth = (int) token.SelectToken("Username").SelectToken("Width");
                UsernameHeight = (int) token.SelectToken("Username").SelectToken("Height");
                UsernameLeft = (int) token.SelectToken("Username").SelectToken("Left");
                UsernameTop = (int) token.SelectToken("Username").SelectToken("Top");

                LocationVisible = (bool) token.SelectToken("Location").SelectToken("Visible");
                Location = token.SelectToken("Location").SelectToken("Text").ToString();
                LocationFontFamily = new FontFamily(token.SelectToken("Location").SelectToken("FontFamily").ToString());
                cfs = new FontStyleConverter().ConvertFromString(token.SelectToken("Location").SelectToken("FontStyle").ToString());
                if (cfs != null) 
                    LocationFontStyle = (FontStyle) cfs;
                cfs = new FontStretchConverter().ConvertFromString(
                    token.SelectToken("Location").SelectToken("FontStretch").ToString());
                if (cfs != null) 
                    LocationFontStretch = (FontStretch) cfs;
                cfs = new FontWeightConverter().ConvertFromString(token.SelectToken("Location").SelectToken("FontWeight").ToString());
                if (cfs != null)
                    LocationFontWeight = (FontWeight) cfs;
                LocationFontSize = (int) token.SelectToken("Location").SelectToken("FontSize");
                LocationForeground = (SolidColorBrush) new BrushConverter().ConvertFrom( token.SelectToken("Location").SelectToken("Foreground").ToString());
                LocationWidth = (int) token.SelectToken("Location").SelectToken("Width");
                LocationHeight = (int) token.SelectToken("Location").SelectToken("Height");
                LocationTop = (int) token.SelectToken("Location").SelectToken("Top");

                CaptionVisible = (bool) token.SelectToken("Caption").SelectToken("Visible");
                CaptionFontFamily = new FontFamily(token.SelectToken("Caption").SelectToken("FontFamily").ToString());
                cfs = new FontStyleConverter().ConvertFromString(
                    token.SelectToken("Caption").SelectToken("FontStyle").ToString());
                if (cfs != null)
                    CaptionFontStyle =
                        (FontStyle)
                            cfs;
                cfs = new FontStretchConverter().ConvertFromString(
                    token.SelectToken("Caption").SelectToken("FontStretch").ToString());
                if (cfs != null)
                    CaptionFontStretch =
                        (FontStretch)
                            cfs;
                cfs = new FontWeightConverter().ConvertFromString(
                    token.SelectToken("Caption").SelectToken("FontWeight").ToString());
                if (cfs != null)
                    CaptionFontWeight =
                        (FontWeight)
                            cfs;
                CaptionFontSize = (int) token.SelectToken("Caption").SelectToken("FontSize");
                CaptionForeground =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("Caption").SelectToken("Foreground").ToString());
                CaptionWidth = (int) token.SelectToken("Caption").SelectToken("Width");
                CaptionHeight = (int) token.SelectToken("Caption").SelectToken("Height");
                CaptionLeft = (int) token.SelectToken("Caption").SelectToken("Left");
                CaptionTop = (int) token.SelectToken("Caption").SelectToken("Top");

                BrandingVisible = (bool) token.SelectToken("Branding").SelectToken("Visible");
                Branding = token.SelectToken("Branding").SelectToken("Text").ToString();
                BrandingFontFamily = new FontFamily(token.SelectToken("Branding").SelectToken("FontFamily").ToString());
                var s2 = new FontStyleConverter().ConvertFromString(
                    token.SelectToken("Branding").SelectToken("FontStyle").ToString());
                if (s2 != null)
                    BrandingFontStyle =
                        (FontStyle)
                            s2;
                var o2 = new FontStretchConverter().ConvertFromString(
                    token.SelectToken("Branding").SelectToken("FontStretch").ToString());
                if (o2 != null)
                    BrandingFontStretch =
                        (FontStretch)
                            o2;
                var fromString2 = new FontWeightConverter().ConvertFromString(
                    token.SelectToken("Branding").SelectToken("FontWeight").ToString());
                if (fromString2 != null)
                    BrandingFontWeight =
                        (FontWeight)
                            fromString2;
                BrandingFontSize = (int) token.SelectToken("Branding").SelectToken("FontSize");
                BrandingForeground =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("Branding").SelectToken("Foreground").ToString());
                BrandingWidth = (int) token.SelectToken("Branding").SelectToken("Width");
                BrandingHeight = (int) token.SelectToken("Branding").SelectToken("Height");
                BrandingLeft = (int) token.SelectToken("Branding").SelectToken("Left");
                BrandingTop = (int) token.SelectToken("Branding").SelectToken("Top");

                CustomTextVisible = (bool) token.SelectToken("CustomText").SelectToken("Visible");
                CustomText = token.SelectToken("CustomText").SelectToken("Text").ToString();
                CustomTextFontFamily =
                    new FontFamily(token.SelectToken("CustomText").SelectToken("FontFamily").ToString());
                cfs = new FontStyleConverter().ConvertFromString(
                    token.SelectToken("CustomText").SelectToken("FontStyle").ToString());
                if (cfs != null)
                    CustomTextFontStyle =
                        (FontStyle)
                            cfs;
                cfs = new FontStretchConverter().ConvertFromString(
                    token.SelectToken("CustomText").SelectToken("FontStretch").ToString());
                if (cfs != null)
                    CustomTextFontStretch =
                        (FontStretch)
                            cfs;
                cfs = new FontWeightConverter().ConvertFromString(
                    token.SelectToken("CustomText").SelectToken("FontWeight").ToString());
                if (cfs != null)
                    CustomTextFontWeight =
                        (FontWeight)
                            cfs;
                CustomTextFontSize = (int) token.SelectToken("CustomText").SelectToken("FontSize");
                CustomTextForeground =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("CustomText").SelectToken("Foreground").ToString());
                CustomTextWidth = (int) token.SelectToken("CustomText").SelectToken("Width");
                CustomTextHeight = (int) token.SelectToken("CustomText").SelectToken("Height");
                CustomTextLeft = (int) token.SelectToken("CustomText").SelectToken("Left");
                CustomTextTop = (int) token.SelectToken("CustomText").SelectToken("Top");

                TimeVisible = (bool)token.SelectToken("Time").SelectToken("Visible");
                TimeFontFamily =
                    new FontFamily(token.SelectToken("Time").SelectToken("FontFamily").ToString());
                cfs = new FontStyleConverter().ConvertFromString(
                    token.SelectToken("Time").SelectToken("FontStyle").ToString());
                if (cfs != null)
                    TimeFontStyle =
                        (FontStyle)
                            cfs;
                cfs = new FontStretchConverter().ConvertFromString(
                    token.SelectToken("Time").SelectToken("FontStretch").ToString());
                if (cfs != null)
                    TimeFontStretch =
                        (FontStretch)
                            cfs;
                cfs = new FontWeightConverter().ConvertFromString(
                    token.SelectToken("Time").SelectToken("FontWeight").ToString());
                if (cfs != null)
                    TimeFontWeight =
                        (FontWeight)
                            cfs;
                TimeFontSize = (int)token.SelectToken("Time").SelectToken("FontSize");
                TimeForeground =
                    (SolidColorBrush)
                        new BrushConverter().ConvertFrom(
                            token.SelectToken("Time").SelectToken("Foreground").ToString());
                TimeWidth = (int)token.SelectToken("Time").SelectToken("Width");
                TimeHeight = (int)token.SelectToken("Time").SelectToken("Height");
                TimeLeft = (int)token.SelectToken("Time").SelectToken("Left");
                TimeTop = (int)token.SelectToken("Time").SelectToken("Top");
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        public PolaroidModel GetPolaroidModel()
        {
            var pm = new PolaroidModel(_polaroidWidth, _polaroidHeight, _polaroidDpi)
            {
                BackgroundColor = _polaroidBackgroundColor,
                BackgroundImage = (BitmapSource) _polaroidBackgroundImage,
                BackgroundImageVisible = _polaroidBackgroundImageVisible,
                Overlay = (BitmapSource) _polaroidOverlay,
                OverlayVisible = _polaroidOverlayVisible,
                Username = new TextModel
                {
                    Visible = _usernameVisible,
                    Text = _username,
                    FontFamily = _usernameFontFamily,
                    FontStyle = _usernameFontStyle,
                    FontWeight = _usernameFontWeight,
                    FontStretch = _usernameFontStretch,
                    FontSize = _usernameFontSize,
                    Foreground = _usernameForeground,
                    Width = _usernameWidth,
                    Height = _usernameHeight,
                    Point = new Point(_usernameLeft, _usernameTop)
                },
                Caption = new TextModel
                {
                    Visible = _captionVisible,
                    Text = _caption,
                    FontFamily = _captionFontFamily,
                    FontStyle = _captionFontStyle,
                    FontWeight = _captionFontWeight,
                    FontStretch = _captionFontStretch,
                    FontSize = _captionFontSize,
                    Foreground = _captionForeground,
                    Width = _captionWidth,
                    Height = _captionHeight,
                    Point = new Point(_captionLeft, _captionTop)
                },
                Location = new TextModel
                {
                    Visible = _locationVisible,
                    Text = _location,
                    FontFamily = _locationFontFamily,
                    FontStyle = _locationFontStyle,
                    FontWeight = _locationFontWeight,
                    FontStretch = _locationFontStretch,
                    FontSize = _locationFontSize,
                    Foreground = _locationForeground,
                    Width = _locationWidth,
                    Height = _locationHeight,
                    Point = new Point(_locationLeft, _locationTop)
                },
                Branding = new TextModel
                {
                    Visible = _brandingVisible,
                    Text = _branding,
                    FontFamily = _brandingFontFamily,
                    FontStyle = _brandingFontStyle,
                    FontWeight = _brandingFontWeight,
                    FontStretch = _brandingFontStretch,
                    FontSize = _brandingFontSize,
                    Foreground = _brandingForeground,
                    Width = _brandingWidth,
                    Height = _brandingHeight,
                    Point = new Point(_brandingLeft, _brandingTop)
                },
                CustomText = new TextModel
                {
                    Visible = _customTextVisible,
                    Text = _customText,
                    FontFamily = _customTextFontFamily,
                    FontStyle = _customTextFontStyle,
                    FontWeight = _customTextFontWeight,
                    FontStretch = _customTextFontStretch,
                    FontSize = _customTextFontSize,
                    Foreground = _customTextForeground,
                    Width = _customTextWidth,
                    Height = _customTextHeight,
                    Point = new Point(_customTextLeft, _customTextTop)
                },
                Time = new TextModel()
                {
                    Visible = _timeVisible,
                    FontFamily = _timeFontFamily,
                    FontStyle = _timeFontStyle,
                    FontWeight = _timeFontWeight,
                    FontStretch = _timeFontStretch,
                    FontSize = _timeFontSize,
                    Foreground = _timeForeground,
                    Width = _timeWidth,
                    Height = _timeHeight,
                    Point = new Point(_timeLeft, _timeTop)
                },
                ProfileImage = new ImageModel
                {
                    Visible = _profileImageVisible,
                    Source = (BitmapSource) _profileImageSource,
                    Width = _profileImageWidth,
                    Height = _profileImageHeight,
                    Left = _profileImageLeft,
                    Top = _profileImageTop
                },
                Image = new ImageModel
                {
                    Visible = _imageVisible,
                    Source = (BitmapSource) _imageSource,
                    Width = _imageWidth,
                    Height = _imageHeight,
                    Left = _imageLeft,
                    Top = _imageTop
                },
                LocationIcon = new ImageModel
                {
                    Visible = _locationIconVisible,
                    Source = (BitmapImage) _locationIconSource,
                    Width = _locationIconWidth,
                    Height = _locationIconHeight,
                    Left = _locationIconLeft,
                    Top = _locationIconTop
                },
                CaptionIcon = new ImageModel
                {
                    Visible = _captionIconVisible,
                    Source = (BitmapSource) _captionIconSource,
                    Width = _captionIconWidth,
                    Height = _captionIconHeight,
                    Left = _captionIconLeft,
                    Top = _captionIconTop
                },
                Logo = new ImageModel
                {
                    Visible = _logoVisible,
                    Source = (BitmapSource) _logoSource,
                    Width = _logoWidth,
                    Height = _logoHeight,
                    Left = _logoLeft,
                    Top = _logoTop
                }
            };

            return pm;
        }

        private void SetPolaroidSingleton(object args)
        {
            PolaroidSingleton.Instance.PolaroidModel = GetPolaroidModel();
        }

        #region Static variables

        private int _polaroidWidth;
        private int _polaroidHeight;
        private int _polaroidDpi;
        private Brush _polaroidBackgroundColor;
        private ImageSource _polaroidBackgroundImage;
        private ImageSource _polaroidOverlay;
        private bool _polaroidBackgroundImageVisible;
        private bool _polaroidOverlayVisible;

        private string _username;
        private int _usernameWidth;
        private int _usernameHeight;
        private int _usernameLeft;
        private int _usernameTop;
        private int _usernameFontSize;
        private FontFamily _usernameFontFamily;
        private FontStyle _usernameFontStyle;
        private FontStretch _usernameFontStretch;
        private FontWeight _usernameFontWeight;
        private Brush _usernameForeground;
        private bool _usernameVisible;

        private string _location;
        private int _locationWidth;
        private int _locationHeight;
        private int _locationLeft;
        private int _locationTop;
        private int _locationFontSize;
        private FontFamily _locationFontFamily;
        private FontStyle _locationFontStyle;
        private FontStretch _locationFontStretch;
        private FontWeight _locationFontWeight;
        private Brush _locationForeground;
        private bool _locationVisible;

        private string _caption;
        private int _captionWidth;
        private int _captionHeight;
        private int _captionLeft;
        private int _captionTop;
        private int _captionFontSize;
        private FontFamily _captionFontFamily;
        private FontStyle _captionFontStyle;
        private FontStretch _captionFontStretch;
        private FontWeight _captionFontWeight;
        private Brush _captionForeground;
        private bool _captionVisible;

        private string _branding;
        private int _brandingWidth;
        private int _brandingHeight;
        private int _brandingLeft;
        private int _brandingTop;
        private int _brandingFontSize;
        private FontFamily _brandingFontFamily;
        private FontStyle _brandingFontStyle;
        private FontStretch _brandingFontStretch;
        private FontWeight _brandingFontWeight;
        private Brush _brandingForeground;
        private bool _brandingVisible;

        private string _customText;
        private int _customTextWidth;
        private int _customTextHeight;
        private int _customTextLeft;
        private int _customTextTop;
        private int _customTextFontSize;
        private FontFamily _customTextFontFamily;
        private FontStyle _customTextFontStyle;
        private FontStretch _customTextFontStretch;
        private FontWeight _customTextFontWeight;
        private Brush _customTextForeground;
        private bool _customTextVisible;

        private int _timeWidth;
        private int _timeHeight;
        private int _timeLeft;
        private int _timeTop;
        private int _timeFontSize;
        private FontFamily _timeFontFamily;
        private FontStyle _timeFontStyle;
        private FontStretch _timeFontStretch;
        private FontWeight _timeFontWeight;
        private Brush _timeForeground;
        private bool _timeVisible;

        private ImageSource _profileImageSource;
        private int _profileImageWidth;
        private int _profileImageHeight;
        private int _profileImageLeft;
        private int _profileImageTop;
        private bool _profileImageVisible;

        private ImageSource _imageSource;
        private int _imageWidth;
        private int _imageHeight;
        private int _imageLeft;
        private int _imageTop;
        private bool _imageVisible;

        private ImageSource _locationIconSource;
        private Brush _locationIconColor;
        private int _locationIconWidth;
        private int _locationIconHeight;
        private int _locationIconLeft;
        private int _locationIconTop;
        private bool _locationIconVisible;

        private ImageSource _captionIconSource;
        private Brush _captionIconColor;
        private int _captionIconWidth;
        private int _captionIconHeight;
        private int _captionIconLeft;
        private int _captionIconTop;
        private bool _captionIconVisible;

        private ImageSource _logoSource;
        private int _logoWidth;
        private int _logoHeight;
        private int _logoLeft;
        private int _logoTop;
        private bool _logoVisible;

        #endregion

        #region NotifyPropertyChange

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public Brush PolaroidBackgroundColor
        {
            get { return _polaroidBackgroundColor; }
            set
            {
                _polaroidBackgroundColor = value;
                NotifyPropertyChange("PolaroidBackgroundColor");
            }
        }

        public ImageSource PolaroidBackgroundImage
        {
            get { return _polaroidBackgroundImage; }
            set
            {
                _polaroidBackgroundImage = value;
                NotifyPropertyChange("PolaroidBackgroundImage");
            }
        }

        public ImageSource PolaroidOverlay
        {
            get { return _polaroidOverlay; }
            set
            {
                _polaroidOverlay = value;
                NotifyPropertyChange("PolaroidOverlay");
            }
        }

        public int PolaroidWidth
        {
            get { return _polaroidWidth; }
            set
            {
                _polaroidWidth = value;
                NotifyPropertyChange("PolaroidWidth");
            }
        }

        public int PolaroidHeight
        {
            get { return _polaroidHeight; }
            set
            {
                _polaroidHeight = value;
                NotifyPropertyChange("PolaroidHeight");
            }
        }

        public int PolaroidDpi
        {
            get { return _polaroidDpi; }
            set
            {
                _polaroidDpi = value;
                NotifyPropertyChange("PolaroidDpi");
            }
        }

        public bool PolaroidBackgroundImageVisible
        {
            get { return _polaroidBackgroundImageVisible; }
            set
            {
                if (!value)
                {
                    PolaroidBackgroundImage = null;
                }
                _polaroidBackgroundImageVisible = value;
                NotifyPropertyChange("PolaroidBackgroundImageVisible");
            }
        }

        public bool PolaroidOverlayVisible
        {
            get { return _polaroidOverlayVisible; }
            set
            {
                if (!value)
                {
                    PolaroidOverlay = null;
                }
                _polaroidOverlayVisible = value;
                NotifyPropertyChange("PolaroidOverlayVisible");
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value;
                NotifyPropertyChange("Username");
            }
        }

        public int UsernameWidth
        {
            get { return _usernameWidth; }
            set
            {
                _usernameWidth = value;
                NotifyPropertyChange("UsernameWidth");
            }
        }

        public int UsernameHeight
        {
            get { return _usernameHeight; }
            set
            {
                _usernameHeight = value;
                NotifyPropertyChange("UsernameHeight");
            }
        }

        public int UsernameLeft
        {
            get { return _usernameLeft; }
            set
            {
                _usernameLeft = value;
                NotifyPropertyChange("UsernameLeft");
            }
        }

        public int UsernameTop
        {
            get { return _usernameTop; }
            set
            {
                _usernameTop = value;
                NotifyPropertyChange("UsernameTop");
            }
        }

        public int UsernameFontSize
        {
            get { return _usernameFontSize; }
            set
            {
                _usernameFontSize = value;
                NotifyPropertyChange("UsernameFontSize");
            }
        }

        public FontFamily UsernameFontFamily
        {
            get { return _usernameFontFamily; }
            set
            {
                _usernameFontFamily = value;
                NotifyPropertyChange("UsernameFontFamily");
            }
        }

        public FontWeight UsernameFontWeight
        {
            get { return _usernameFontWeight; }
            set
            {
                _usernameFontWeight = value;
                NotifyPropertyChange("UsernameFontWeight");
            }
        }

        public FontStyle UsernameFontStyle
        {
            get { return _usernameFontStyle; }
            set
            {
                _usernameFontStyle = value;
                NotifyPropertyChange("UsernameFontStyle");
            }
        }

        public FontStretch UsernameFontStretch
        {
            get { return _usernameFontStretch; }
            set
            {
                _usernameFontStretch = value;
                NotifyPropertyChange("UsernameFontStretch");
            }
        }

        public Brush UsernameForeground
        {
            get { return _usernameForeground; }
            set
            {
                _usernameForeground = value;
                NotifyPropertyChange("UsernameForeground");
            }
        }

        public bool UsernameVisible
        {
            get { return _usernameVisible; }
            set
            {
                _usernameVisible = value;
                NotifyPropertyChange("UsernameVisible");
            }
        }

        public string Location
        {
            get { return _location; }
            set
            {
                _location = value;
                NotifyPropertyChange("Location");
            }
        }

        public int LocationWidth
        {
            get { return _locationWidth; }
            set
            {
                _locationWidth = value;
                NotifyPropertyChange("UsernameWidth");
            }
        }

        public int LocationHeight
        {
            get { return _locationHeight; }
            set
            {
                _locationHeight = value;
                NotifyPropertyChange("LocationHeight");
            }
        }

        public int LocationLeft
        {
            get { return _locationLeft; }
            set
            {
                _locationLeft = value;
                NotifyPropertyChange("LocationLeft");
            }
        }

        public int LocationTop
        {
            get { return _locationTop; }
            set
            {
                _locationTop = value;
                NotifyPropertyChange("LocationTop");
            }
        }

        public int LocationFontSize
        {
            get { return _locationFontSize; }
            set
            {
                _locationFontSize = value;
                NotifyPropertyChange("LocationFontSize");
            }
        }

        public FontFamily LocationFontFamily
        {
            get { return _locationFontFamily; }
            set
            {
                _locationFontFamily = value;
                NotifyPropertyChange("LocationFontFamily");
            }
        }

        public FontWeight LocationFontWeight
        {
            get { return _locationFontWeight; }
            set
            {
                _locationFontWeight = value;
                NotifyPropertyChange("LocationFontWeight");
            }
        }

        public FontStyle LocationFontStyle
        {
            get { return _locationFontStyle; }
            set
            {
                _locationFontStyle = value;
                NotifyPropertyChange("LocationFontStyle");
            }
        }

        public FontStretch LocationFontStretch
        {
            get { return _locationFontStretch; }
            set
            {
                _locationFontStretch = value;
                NotifyPropertyChange("LocationFontStretch");
            }
        }

        public Brush LocationForeground
        {
            get { return _locationForeground; }
            set
            {
                _locationForeground = value;
                NotifyPropertyChange("LocationForeground");
            }
        }

        public bool LocationVisible
        {
            get { return _locationVisible; }
            set
            {
                _locationVisible = value;
                NotifyPropertyChange("LocationVisible");
            }
        }

        public string Caption
        {
            get { return _caption; }
            set
            {
                _caption = value;
                NotifyPropertyChange("Caption");
            }
        }

        public int CaptionWidth
        {
            get { return _captionWidth; }
            set
            {
                _captionWidth = value;
                NotifyPropertyChange("CaptionWidth");
            }
        }

        public int CaptionHeight
        {
            get { return _captionHeight; }
            set
            {
                _captionHeight = value;
                NotifyPropertyChange("CaptionHeight");
            }
        }

        public int CaptionLeft
        {
            get { return _captionLeft; }
            set
            {
                _captionLeft = value;
                NotifyPropertyChange("CaptionLeft");
            }
        }

        public int CaptionTop
        {
            get { return _captionTop; }
            set
            {
                _captionTop = value;
                NotifyPropertyChange("CaptionTop");
            }
        }

        public int CaptionFontSize
        {
            get { return _captionFontSize; }
            set
            {
                _captionFontSize = value;
                NotifyPropertyChange("CaptionFontSize");
            }
        }

        public FontFamily CaptionFontFamily
        {
            get { return _captionFontFamily; }
            set
            {
                _captionFontFamily = value;
                NotifyPropertyChange("CaptionFontFamily");
            }
        }

        public FontWeight CaptionFontWeight
        {
            get { return _captionFontWeight; }
            set
            {
                _captionFontWeight = value;
                NotifyPropertyChange("CaptionFontWeight");
            }
        }

        public FontStyle CaptionFontStyle
        {
            get { return _captionFontStyle; }
            set
            {
                _captionFontStyle = value;
                NotifyPropertyChange("CaptionFontStyle");
            }
        }

        public FontStretch CaptionFontStretch
        {
            get { return _captionFontStretch; }
            set
            {
                _captionFontStretch = value;
                NotifyPropertyChange("CaptionFontStretch");
            }
        }

        public Brush CaptionForeground
        {
            get { return _captionForeground; }
            set
            {
                _captionForeground = value;
                NotifyPropertyChange("CaptionForeground");
            }
        }

        public bool CaptionVisible
        {
            get { return _captionVisible; }
            set
            {
                _captionVisible = value;
                NotifyPropertyChange("CaptionVisible");
            }
        }        

        public string Branding
        {
            get { return _branding; }
            set
            {
                _branding = value;
                NotifyPropertyChange("Branding");
            }
        }

        public int BrandingWidth
        {
            get { return _brandingWidth; }
            set
            {
                _brandingWidth = value;
                NotifyPropertyChange("BrandingWidth");
            }
        }

        public int BrandingHeight
        {
            get { return _brandingHeight; }
            set
            {
                _brandingHeight = value;
                NotifyPropertyChange("BrandingHeight");
            }
        }

        public int BrandingLeft
        {
            get { return _brandingLeft; }
            set
            {
                _brandingLeft = value;
                NotifyPropertyChange("BrandingLeft");
            }
        }

        public int BrandingTop
        {
            get { return _brandingTop; }
            set
            {
                _brandingTop = value;
                NotifyPropertyChange("BrandingTop");
            }
        }

        public int BrandingFontSize
        {
            get { return _brandingFontSize; }
            set
            {
                _brandingFontSize = value;
                NotifyPropertyChange("BrandingFontSize");
            }
        }

        public FontFamily BrandingFontFamily
        {
            get { return _brandingFontFamily; }
            set
            {
                _brandingFontFamily = value;
                NotifyPropertyChange("BrandingFontFamily");
            }
        }

        public FontWeight BrandingFontWeight
        {
            get { return _brandingFontWeight; }
            set
            {
                _brandingFontWeight = value;
                NotifyPropertyChange("BrandingFontWeight");
            }
        }

        public FontStyle BrandingFontStyle
        {
            get { return _brandingFontStyle; }
            set
            {
                _brandingFontStyle = value;
                NotifyPropertyChange("BrandingFontStyle");
            }
        }

        public FontStretch BrandingFontStretch
        {
            get { return _brandingFontStretch; }
            set
            {
                _brandingFontStretch = value;
                NotifyPropertyChange("BrandingFontStretch");
            }
        }

        public Brush BrandingForeground
        {
            get { return _brandingForeground; }
            set
            {
                _brandingForeground = value;
                NotifyPropertyChange("BrandingForeground");
            }
        }

        public bool BrandingVisible
        {
            get { return _brandingVisible; }
            set
            {
                _brandingVisible = value;
                NotifyPropertyChange("BrandingVisible");
            }
        }

        public string CustomText
        {
            get { return _customText; }
            set
            {
                _customText = value;
                NotifyPropertyChange("CustomText");
            }
        }

        public int CustomTextWidth
        {
            get { return _customTextWidth; }
            set
            {
                _customTextWidth = value;
                NotifyPropertyChange("CustomTextWidth");
            }
        }

        public int CustomTextHeight
        {
            get { return _customTextHeight; }
            set
            {
                _customTextHeight = value;
                NotifyPropertyChange("CustomTextHeight");
            }
        }

        public int CustomTextLeft
        {
            get { return _customTextLeft; }
            set
            {
                _customTextLeft = value;
                NotifyPropertyChange("CustomTextLeft");
            }
        }

        public int CustomTextTop
        {
            get { return _customTextTop; }
            set
            {
                _customTextTop = value;
                NotifyPropertyChange("CustomTextTop");
            }
        }

        public int CustomTextFontSize
        {
            get { return _customTextFontSize; }
            set
            {
                _customTextFontSize = value;
                NotifyPropertyChange("CustomTextFontSize");
            }
        }

        public FontFamily CustomTextFontFamily
        {
            get { return _customTextFontFamily; }
            set
            {
                _customTextFontFamily = value;
                NotifyPropertyChange("CustomTextFontFamily");
            }
        }

        public FontWeight CustomTextFontWeight
        {
            get { return _customTextFontWeight; }
            set
            {
                _customTextFontWeight = value;
                NotifyPropertyChange("CustomTextFontWeight");
            }
        }

        public FontStyle CustomTextFontStyle
        {
            get { return _customTextFontStyle; }
            set
            {
                _customTextFontStyle = value;
                NotifyPropertyChange("CustomTextFontStyle");
            }
        }

        public FontStretch CustomTextFontStretch
        {
            get { return _customTextFontStretch; }
            set
            {
                _customTextFontStretch = value;
                NotifyPropertyChange("CustomTextFontStretch");
            }
        }

        public Brush CustomTextForeground
        {
            get { return _customTextForeground; }
            set
            {
                _customTextForeground = value;
                NotifyPropertyChange("CustomTextForeground");
            }
        }

        public bool CustomTextVisible
        {
            get { return _customTextVisible; }
            set
            {
                _customTextVisible = value;
                NotifyPropertyChange("CustomTextVisible");
            }
        }

        public string Time
        {
            get { return string.Format("{0:hh:mm tt}", DateTime.Now); }
        }

        public int TimeWidth
        {
            get { return _timeWidth; }
            set
            {
                _timeWidth = value;
                NotifyPropertyChange("TimeWidth");
            }
        }

        public int TimeHeight
        {
            get { return _timeHeight; }
            set
            {
                _timeHeight = value;
                NotifyPropertyChange("TimeHeight");
            }
        }

        public int TimeLeft
        {
            get { return _timeLeft; }
            set
            {
                _timeLeft = value;
                NotifyPropertyChange("TimeLeft");
            }
        }

        public int TimeTop
        {
            get { return _timeTop; }
            set
            {
                _timeTop = value;
                NotifyPropertyChange("TimeTop");
            }
        }

        public int TimeFontSize
        {
            get { return _timeFontSize; }
            set
            {
                _timeFontSize = value;
                NotifyPropertyChange("TimeFontSize");
            }
        }

        public FontFamily TimeFontFamily
        {
            get { return _timeFontFamily; }
            set
            {
                _timeFontFamily = value;
                NotifyPropertyChange("TimeFontFamily");
            }
        }

        public FontWeight TimeFontWeight
        {
            get { return _timeFontWeight; }
            set
            {
                _timeFontWeight = value;
                NotifyPropertyChange("TimeFontWeight");
            }
        }

        public FontStyle TimeFontStyle
        {
            get { return _timeFontStyle; }
            set
            {
                _timeFontStyle = value;
                NotifyPropertyChange("TimeFontStyle");
            }
        }

        public FontStretch TimeFontStretch
        {
            get { return _timeFontStretch; }
            set
            {
                _timeFontStretch = value;
                NotifyPropertyChange("TimeFontStretch");
            }
        }

        public Brush TimeForeground
        {
            get { return _timeForeground; }
            set
            {
                _timeForeground = value;
                NotifyPropertyChange("TimeForeground");
            }
        }

        public bool TimeVisible
        {
            get { return _timeVisible; }
            set
            {
                _timeVisible = value;
                NotifyPropertyChange("TimeVisible");
            }
        }

        public ImageSource ProfileImageSource
        {
            get { return _profileImageSource; }
            set
            {
                _profileImageSource = value;
                NotifyPropertyChange("ProfileImageSource");
            }
        }

        public int ProfileImageWidth
        {
            get { return _profileImageWidth; }
            set
            {
                _profileImageWidth = value;
                NotifyPropertyChange("ProfileImageWidth");
            }
        }

        public int ProfileImageHeight
        {
            get { return _profileImageHeight; }
            set
            {
                _profileImageHeight = value;
                NotifyPropertyChange("ProfileImageHeight");
            }
        }

        public int ProfileImageLeft
        {
            get { return _profileImageLeft; }
            set
            {
                _profileImageLeft = value;
                NotifyPropertyChange("ProfileImageLeft");
            }
        }

        public int ProfileImageTop
        {
            get { return _profileImageTop; }
            set
            {
                _profileImageTop = value;
                NotifyPropertyChange("ProfileImageTop");
            }
        }

        public bool ProfileImageVisible
        {
            get { return _profileImageVisible; }
            set
            {
                _profileImageVisible = value;
                NotifyPropertyChange("ProfileImageVisible");
            }
        }

        public ImageSource ImageSource
        {
            get { return _imageSource; }
            set
            {
                _imageSource = value;
                NotifyPropertyChange("ImageSource");
            }
        }

        public int ImageWidth
        {
            get { return _imageWidth; }
            set
            {
                _imageWidth = value;
                NotifyPropertyChange("ImageWidth");
            }
        }

        public int ImageHeight
        {
            get { return _imageHeight; }
            set
            {
                _imageHeight = value;
                NotifyPropertyChange("ImageHeight");
            }
        }

        public int ImageLeft
        {
            get { return _imageLeft; }
            set
            {
                _imageLeft = value;
                NotifyPropertyChange("ImageLeft");
            }
        }

        public int ImageTop
        {
            get { return _imageTop; }
            set
            {
                _imageTop = value;
                NotifyPropertyChange("ImageTop");
            }
        }

        public bool ImageVisible
        {
            get { return _imageVisible; }
            set
            {
                _imageVisible = value;
                NotifyPropertyChange("ImageVisible");
            }
        }

        public ImageSource LocationIconSource
        {
            get { return _locationIconSource; }
            set
            {
                _locationIconSource = value;
                NotifyPropertyChange("LocationIconSource");
            }
        }

        public Brush LocationIconColor
        {
            get { return _locationIconColor; }
            set
            {
                _locationIconColor = value;
                NotifyPropertyChange("LocationIconColor");
            }
        }

        public int LocationIconWidth
        {
            get { return _locationIconWidth; }
            set
            {
                _locationIconWidth = value;
                NotifyPropertyChange("LocationIconWidth");
            }
        }

        public int LocationIconHeight
        {
            get { return _locationIconHeight; }
            set
            {
                _locationIconHeight = value;
                NotifyPropertyChange("LocationIconHeight");
            }
        }

        public int LocationIconLeft
        {
            get { return _locationIconLeft; }
            set
            {
                _locationIconLeft = value;
                NotifyPropertyChange("LocationIconLeft");
            }
        }

        public int LocationIconTop
        {
            get { return _locationIconTop; }
            set
            {
                _locationIconTop = value;
                NotifyPropertyChange("LocationIconTop");
            }
        }

        public bool LocationIconVisible
        {
            get { return _locationIconVisible; }
            set
            {
                _locationIconVisible = value;
                NotifyPropertyChange("LocationIconVisible");
            }
        }


        public ImageSource CaptionIconSource
        {
            get { return _captionIconSource; }
            set
            {
                _captionIconSource = value;
                NotifyPropertyChange("CaptionIconSource");
            }
        }

        public Brush CaptionIconColor
        {
            get { return _captionIconColor; }
            set
            {
                _captionIconColor = value;
                NotifyPropertyChange("CaptionIconColor");
            }
        }

        public int CaptionIconWidth
        {
            get { return _captionIconWidth; }
            set
            {
                _captionIconWidth = value;
                NotifyPropertyChange("CaptionIconWidth");
            }
        }

        public int CaptionIconHeight
        {
            get { return _captionIconHeight; }
            set
            {
                _captionIconHeight = value;
                NotifyPropertyChange("CaptionIconHeight");
            }
        }

        public int CaptionIconLeft
        {
            get { return _captionIconLeft; }
            set
            {
                _captionIconLeft = value;
                NotifyPropertyChange("CaptionIconLeft");
            }
        }

        public int CaptionIconTop
        {
            get { return _captionIconTop; }
            set
            {
                _captionIconTop = value;
                NotifyPropertyChange("CaptionIconTop");
            }
        }

        public bool CaptionIconVisible
        {
            get { return _captionIconVisible; }
            set
            {
                _captionIconVisible = value;
                NotifyPropertyChange("CaptionIconVisible");
            }
        }

        public ImageSource LogoSource
        {
            get { return _logoSource; }
            set
            {
                _logoSource = value;
                NotifyPropertyChange("LogoSource");
            }
        }

        public int LogoWidth
        {
            get { return _logoWidth; }
            set
            {
                _logoWidth = value;
                NotifyPropertyChange("LogoWidth");
            }
        }

        public int LogoHeight
        {
            get { return _logoHeight; }
            set
            {
                _logoHeight = value;
                NotifyPropertyChange("LogoHeight");
            }
        }

        public int LogoLeft
        {
            get { return _logoLeft; }
            set
            {
                _logoLeft = value;
                NotifyPropertyChange("LogoLeft");
            }
        }

        public int LogoTop
        {
            get { return _logoTop; }
            set
            {
                _logoTop = value;
                NotifyPropertyChange("LogoTop");
            }
        }

        public bool LogoVisible
        {
            get { return _logoVisible; }
            set
            {
                if (!value)
                {
                    LogoSource = null;
                }
                _logoVisible = value;
                NotifyPropertyChange("LogoVisible");
            }
        }


        public bool ShowAdornerDesigner
        {
            get { return _showAdornerDesigner; }
            set
            {
                _showAdornerDesigner = value;
                NotifyPropertyChange("ShowAdornerDesigner");
            }
        }

        public bool ShowGridLines
        {
            get { return _showGridLines; }
            set
            {
                _showGridLines = value;
                NotifyPropertyChange("ShowGridLines");
            }
        }

        #endregion
    }
}