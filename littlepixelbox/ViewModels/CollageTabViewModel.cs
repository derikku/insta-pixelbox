﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Reflection;
using littlepixelbox.Collage;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using Microsoft.Owin.Hosting;
using NLog;

namespace littlepixelbox.ViewModels
{
    internal class CollageTabViewModel : ViewModelBase
    {
        private readonly BackgroundWorker _bgWrkStartCollage = new BackgroundWorker();
        private string _collageUrl;
        private string _kioskUrl;

        private bool _enable = true;
        private bool _enableGui = true;

        private int _imgPerRow = 5;
        private int _scrollSpeed = 20;
        private readonly Logger _log = LogManager.GetLogger("Collage");

        private IDisposable _webApp;

        public CollageTabViewModel()
        {
            Mediator.Register("RunCollageService", RunCollageService);

            // Hook up background worker
            _bgWrkStartCollage.DoWork += bgWrkStartCollage_DoWork;

            // Set the default values for CollageSingleton
            CollageSingleton.Instance.ImgRow = _imgPerRow;
            CollageSingleton.Instance.ScrollSpeed = _scrollSpeed;

            int? port = !string.IsNullOrEmpty(AppSettings.Get<string>("CollagePort")) ? AppSettings.Get<int>("CollagePort") : 80;

            _collageUrl = port == 80 ? string.Format("http://{0}/collage", Utils.LocalIpAddress()) : string.Format("http://{0}:{1}/collage", Utils.LocalIpAddress(), port);
            _kioskUrl = port == 80 ? string.Format("http://{0}/kiosk", Utils.LocalIpAddress()) : string.Format("http://{0}:{1}/kiosk", Utils.LocalIpAddress(), port);
        }

        public void RunCollageService(object args)
        {
            EnableGui = false;

            if ((bool) args)
            {
                if (Enable)
                {
                    _bgWrkStartCollage.RunWorkerAsync();
                }
            }
            else
            {
                if (Enable)
                {
                    try
                    {
                        if (_webApp != null)
                        {
                            _webApp.Dispose();
                            _log.Info("Collage stopped.");
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.ToString());
                        Mediator.NotifyColleagues("ErrorCount", 1);
                        Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                    }
                }
                EnableGui = true;
            }
        }

        private void bgWrkStartCollage_DoWork(object sender, DoWorkEventArgs e)
        {
            var pixelBoxCollagePort = AppSettings.Get<string>("CollagePort");
            var url = string.Format("http://+:{0}/", pixelBoxCollagePort);

            try
            {
                _webApp = WebApp.Start<Startup>(url);
                _log.Info("Collage started.");
            }
            catch (TargetInvocationException tiex)
            {
                if (tiex.HResult == -2146232828) //Access is denied
                {
                    var args = string.Format(@"http add urlacl url={0} user=everyone", url);
                    var psi = new ProcessStartInfo("netsh", args)
                    {
                        Verb = "runas",
                        CreateNoWindow = true,
                        WindowStyle = ProcessWindowStyle.Hidden,
                        UseShellExecute = true
                    };

                    try
                    {
                        using (var process = Process.Start(psi))
                        {
                            if (process != null) 
                                process.WaitForExit();
                            _webApp = WebApp.Start<Startup>(url);
                        }
                    }
                    catch (Win32Exception ex)
                    {
                        if (ex.NativeErrorCode == 1223)
                        {
                            _log.Error("Please select Yes to add the HTTP URL reservation.");
                            Mediator.NotifyColleagues("ErrorCount", 1);
                            Mediator.NotifyColleagues("ErrorMessage",
                                "Please select Yes to add the HTTP URL reservation.");
                        }
                        else
                        {
                            _log.Error(ex.ToString());
                            Mediator.NotifyColleagues("ErrorCount", 1);
                            Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                        }
                        Enable = false;
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.ToString());
                        Mediator.NotifyColleagues("ErrorCount", 1);
                        Mediator.NotifyColleagues("ErrorMessage", ex.Message);
                    }
                }
                else
                {
                    Enable = false;
                    _log.Error(tiex.InnerException.Message);
                    Mediator.NotifyColleagues("ErrorCount", 1);
                    Mediator.NotifyColleagues("ErrorMessage", tiex.Message);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Enable = false;
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public bool Enable
        {
            get { return _enable; }
            set
            {
                _enable = value;
                NotifyPropertyChange("Enable");
            }
        }

        public string CollageUrl
        {
            get { return _collageUrl; }
            set
            {
                _collageUrl = value;
                NotifyPropertyChange("CollageUrl");
            }
        }

        public string KioskUrl
        {
            get { return _kioskUrl; }
            set
            {
                _kioskUrl = value;
                NotifyPropertyChange("KioskUrl");
            }
        }

        public int ImgPerRow
        {
            get { return _imgPerRow; }
            set
            {
                _imgPerRow = value;
                CollageSingleton.Instance.ImgRow = value;
                NotifyPropertyChange("ImgPerRow");
            }
        }

        public int ScrollSpeed
        {
            get { return _scrollSpeed; }
            set
            {
                _scrollSpeed = value;
                CollageSingleton.Instance.ScrollSpeed = value;
                NotifyPropertyChange("ScrollSpeed");
            }
        }
    }
}