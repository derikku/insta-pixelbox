﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Windows.Threading;
using Dapper;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Core.Wrapper;
using littlepixelbox.Models;
using NLog;

namespace littlepixelbox.ViewModels
{
    internal class ToolsTabViewModel : ViewModelBase
    {
        private readonly BackgroundWorker _bgWrkExportPolaroids = new BackgroundWorker
        {
            WorkerSupportsCancellation = true
        };

        private readonly Logger _log = LogManager.GetLogger("LittlePixelBox");

        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private readonly ObservableCollection<StringValue> _userBanList = new ObservableCollection<StringValue>();

        private bool _enableExport;
        private bool _enableGui = true;
        private string _exportFolder;
        private StringValue _selectedItem;

        private string _username;

        public ToolsTabViewModel()
        {
            Mediator.Register("RunExportService", RunExportService);
            ExportFolder = !string.IsNullOrEmpty(AppSettings.Get<string>("DefaultExportFolder")) ? AppSettings.Get<string>("DefaultExportFolder") : "";

            // Hook up the timer
            _timer.Tick += timer_Tick;
            _timer.Interval = TimeSpan.FromSeconds(1);

            // Hook up backgroundworker
            _bgWrkExportPolaroids.DoWork += bgWrkExportPolaroids_DoWork;

            DeleteCommand = new DelegateCommand(
                DeleteBanUser,
                () => true);

            AddCommand = new DelegateCommand(
                AddBanUser,
                () => !string.IsNullOrEmpty(Username));

            Database.CreateBanListDatabase();

            List<string> userBanList;

            using (var conn = Database.GetBanListConnection())
            {
                var queryString = "SELECT * FROM BanList";
                userBanList = conn.Query<string>(queryString, new {}).ToList();
            }

            foreach (var s in userBanList)
            {
                UserBanList.Add(new StringValue(s));
            }
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public bool EnableExport
        {
            get { return _enableExport; }
            set
            {
                if (!string.IsNullOrEmpty(ExportFolder))
                {
                    _enableExport = value;
                }
                NotifyPropertyChange("EnableExport");
            }
        }

        public string ExportFolder
        {
            get { return _exportFolder; }
            set
            {
                _exportFolder = value;
                NotifyPropertyChange("ExportFolder");
            }
        }

        public ObservableCollection<StringValue> UserBanList
        {
            get { return _userBanList; }
        }

        public StringValue SelectedItem
        {
            get { return _selectedItem; }
            set
            {
                _selectedItem = value;
                NotifyPropertyChange("SelectedItem");
            }
        }

        public string Username
        {
            get { return _username; }
            set
            {
                _username = value.Trim();
                NotifyPropertyChange("Username");
                AddCommand.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand DeleteCommand { get; set; }
        public DelegateCommand AddCommand { get; set; }

        public void RunExportService(object args)
        {
            EnableGui = false;

            if ((bool) args)
            {
                if (EnableExport)
                {
                    _timer.Start();
                }
            }
            else
            {
                _timer.Stop();
                EnableGui = true;
            }
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if (!_bgWrkExportPolaroids.IsBusy)
            {
                _bgWrkExportPolaroids.RunWorkerAsync();
            }
        }

        private void bgWrkExportPolaroids_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                CopyPolaroidImages();
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void CopyPolaroidImages()
        {
            List<DataModel> data;

            using (var conn = Database.GetConnection())
            {
                const string queryString = "SELECT * FROM Data WHERE Exported = 0 AND PolaroidLocation <> ''";
                data = conn.Query<DataModel>(queryString, new {}).ToList();
            }

            foreach (var d in data)
            {
                if (!Utils.IsFileLocked(new FileInfo(d.PolaroidLocation)))
                {
                    var polaroidFilename = Path.GetFileName(d.PolaroidLocation);

                    try
                    {
                        if (polaroidFilename != null)
                            File.Copy(d.PolaroidLocation, Path.Combine(_exportFolder, polaroidFilename), true);

                        using (var conn = Database.GetConnection())
                        {
                            const string queryString = "UPDATE Data SET Exported = 1 WHERE MediaId = @mediaId";
                            conn.Query<DataModel>(queryString, new {mediaId = d.MediaId});
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(ex.ToString());
                    }
                }
            }
        }

        public byte[] ImageToByteArray(Image imageIn)
        {
            using (var ms = new MemoryStream())
            {
                imageIn.Save(ms, ImageFormat.Jpeg);
                return ms.ToArray();
            }
        }

        private void DeleteBanUser()
        {
            if (null != SelectedItem)
            {
                using (var conn = Database.GetBanListConnection())
                {
                    var queryString = "DELETE FROM BanList WHERE Username = @User";
                    conn.Query<int>(queryString, new {User = SelectedItem.Value});

                    UserBanList.Remove(SelectedItem);
                }
            }
        }

        private void AddBanUser()
        {
            Database.CreateBanListDatabase();

            var contains = UserBanList.Any(s => s.Value.Equals(Username));

            const string queryString = "INSERT INTO BanList VALUES (@User)";
            const string queryStringData = "DELETE FROM data WHERE Username = @User";
            if (!string.IsNullOrEmpty(Username) && !contains)
            {
                using (var conn = Database.GetBanListConnection())
                {
                    conn.Query<int>(queryString, new {User = Username});

                    UserBanList.Add(new StringValue(Username));

                    using (var connData = Database.GetConnection())
                    {
                        try
                        {
                            connData.Query<int>(queryStringData, new { User = Username });
                        }
                        catch (Exception ex)
                        {
                            // ignored
                        }
                    }

                    CollageSingleton.Instance.ReloadClients = true;
                }
            }
            Username = string.Empty;
        }
    }
}