﻿using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;

namespace littlepixelbox.ViewModels
{
    internal class StatusBarViewModel : ViewModelBase
    {
        private int _downloadCount;
        private int _errorCount;
        private string _errorMessage = string.Empty;

        private bool _errorVisible;
        private int _imageCreatedCount;
        private int _instagramCount;
        private int _printCount;
        private string _statusText = "Ready";
        private int _videoCreatedCount;
        private int _webRequestCount;

        public StatusBarViewModel()
        {
            Mediator.Register("Status", UpdateStatusText);
            Mediator.Register("WebRequestCount", UpdateWebRequestCount);
            Mediator.Register("InstagramCount", UpdateInstagramCount);
            Mediator.Register("DownloadCount", UpdateDownloadCount);
            Mediator.Register("ImageCount", UpdateImageCount);
            Mediator.Register("VideoCount", UpdateVideoCount);
            Mediator.Register("PrintCount", UpdatePrintCount);
            Mediator.Register("ErrorCount", UpdateErrorCount);
            Mediator.Register("ErrorMessage", UpdateErrorMessage);
        }

        public void UpdateStatusText(object args)
        {
            StatusText = (string) args;
        }

        public void UpdateWebRequestCount(object args)
        {
            WebRequestCount = (int)args;
            CollageSingleton.Instance.WebRequestCount = WebRequestCount;
        }

        public void UpdateInstagramCount(object args)
        {
            InstagramCount = (int) args;
        }

        public void UpdateDownloadCount(object args)
        {
            DownloadCount = (int) args;
        }

        public void UpdateImageCount(object args)
        {
            ImageCreatedCount = (int) args;
        }

        public void UpdateVideoCount(object args)
        {
            VideoCreatedCount = (int) args;
        }

        public void UpdatePrintCount(object args)
        {
            PrintCount = (int) args;
            CollageSingleton.Instance.PrintCount = PrintCount;
        }

        public void UpdateErrorCount(object args)
        {
            ErrorCount = (int) args;
            CollageSingleton.Instance.ErrorCount = ErrorCount;
        }

        public void UpdateErrorMessage(object args)
        {
            ErrorMessage = Utils.RemoveLineEndings((string) args);
        }

        #region NotifyPropertyChange

        public string StatusText
        {
            get { return _statusText; }
            set
            {
                if (_statusText.Equals("Ready"))
                {
                    _webRequestCount = 0;
                    NotifyPropertyChange("WebRequestCount");

                    _instagramCount = 0;
                    NotifyPropertyChange("InstagramCount");

                    _downloadCount = 0;
                    NotifyPropertyChange("DownloadCount");

                    _imageCreatedCount = 0;
                    NotifyPropertyChange("ImageCreatedCount");

                    _videoCreatedCount = 0;
                    NotifyPropertyChange("VideoCreatedCount");

                    _printCount = 0;
                    NotifyPropertyChange("PrintCount");

                    _errorCount = 0;
                    NotifyPropertyChange("ErrorCount");

                    ErrorVisible = false;

                    ErrorMessage = string.Empty;
                }
                _statusText = value;
                NotifyPropertyChange("StatusText");
            }
        }

        public int WebRequestCount
        {
            get { return _webRequestCount; }
            set
            {
                _webRequestCount += value;
                NotifyPropertyChange("WebRequestCount");
            }
        }

        public int InstagramCount
        {
            get { return _instagramCount; }
            set
            {
                _instagramCount += value;
                NotifyPropertyChange("InstagramCount");
            }
        }

        public int DownloadCount
        {
            get { return _downloadCount; }
            set
            {
                _downloadCount += value;
                NotifyPropertyChange("DownloadCount");
            }
        }

        public int ImageCreatedCount
        {
            get { return _imageCreatedCount; }
            set
            {
                _imageCreatedCount += value;
                NotifyPropertyChange("ImageCreatedCount");
            }
        }

        public int VideoCreatedCount
        {
            get { return _videoCreatedCount; }
            set
            {
                _videoCreatedCount += value;
                NotifyPropertyChange("VideoCreatedCount");
            }
        }

        public int PrintCount
        {
            get { return _printCount; }
            set
            {
                _printCount += value;
                NotifyPropertyChange("PrintCount");
            }
        }

        public int ErrorCount
        {
            get { return _errorCount; }
            set
            {
                if (value > 0)
                {
                    ErrorVisible = true;
                }
                _errorCount += value;
                NotifyPropertyChange("ErrorCount");
            }
        }

        public string ErrorMessage
        {
            get { return _errorMessage; }
            set
            {
                if (!string.IsNullOrEmpty(value))
                {
                    ErrorVisible = true;
                }
                _errorMessage = value;
                NotifyPropertyChange("ErrorMessage");
            }
        }

        public bool ErrorVisible
        {
            get { return _errorVisible; }
            set
            {
                _errorVisible = value;
                NotifyPropertyChange("ErrorVisible");
            }
        }

        #endregion
    }
}