﻿using System;
using System.ComponentModel;
using System.IO;
using System.Windows.Threading;
using littlepixelbox.Collage.SignalRHubs;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Views;

namespace littlepixelbox.ViewModels
{
    internal class MainTabViewModel : ViewModelBase
    {
        private readonly BackgroundWorker _bgWrkInstagram = new BackgroundWorker
        {
            WorkerSupportsCancellation = true,
            WorkerReportsProgress = true
        };

        private bool _buildIgnoreList;
        private bool _enableGui = true;
        private DateTime _endDate = DateTime.Now.AddDays(1).AddSeconds(-1);
        private string _hashtag = AppSettings.Get<string>("DefaultHashTag");
        private string _hashtag2 = string.Empty;
        private int _hours;

        private InstagramService _instagram;
        private int _minutes;
        private int _seconds;
        private DateTime _startDate = DateTime.Now.AddYears(-1);

        private readonly DispatcherTimer _timer = new DispatcherTimer();
        private bool _timerEnable;

        public MainTabViewModel()
        {
            // Hook up the background worker
            _bgWrkInstagram.DoWork += bgWrkInstagram_DoWork;
            _bgWrkInstagram.RunWorkerCompleted += bgWrkInstagram_RunWorkerCompleted;

            // Hook up the timer
            _timer.Tick += timer_Tick;
            _timer.Interval = TimeSpan.FromSeconds(1);

            StartCommand = new DelegateCommand(
                () =>
                {
                    if (string.IsNullOrEmpty(InstagramAccessTokenSingleton.Instance.AccessToken))
                    {
                        var il = new InstagramLogin();
                        il.ShowDialog();
                    }
                    else
                    {
                        DefaultPathSingleton.Instance.FolderPath = Path.Combine("Instagram", string.IsNullOrEmpty(Hashtag2) ? Hashtag : string.Format("{0}_{1}", Hashtag, Hashtag2));

                        if (TimerEnable)
                        {
                            _timer.Start();
                        }

                        Database.CreateDatabase();
                        _bgWrkInstagram.RunWorkerAsync();

                        Mediator.NotifyColleagues("RunCreatePolaroidService", true);
                        Mediator.NotifyColleagues("RunPrintPolaroidService", true);
                        Mediator.NotifyColleagues("RunCollageService", true);
                        Mediator.NotifyColleagues("RunExportService", true);

                        Mediator.NotifyColleagues("Status", "Running . . .");
                        EnableGui = false;
                    }
                },
                () => !string.IsNullOrEmpty(_hashtag));

            StopCommand = new DelegateCommand(
                () =>
                {
                    _timer.Stop();
                    _bgWrkInstagram.CancelAsync();
                    _instagram.Stop();

                    Mediator.NotifyColleagues("RunCreatePolaroidService", false);
                    Mediator.NotifyColleagues("RunPrintPolaroidService", false);
                    Mediator.NotifyColleagues("RunCollageService", false);
                    Mediator.NotifyColleagues("RunExportService", false);

                    Mediator.NotifyColleagues("Status", "Stopping . . .");
                },
                () => true);
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public string Hashtag
        {
            get { return _hashtag.Replace("#", ""); }
            set
            {
                _hashtag = value;
                NotifyPropertyChange("Hashtag");
                StartCommand.RaiseCanExecuteChanged();
            }
        }

        public string Hashtag2
        {
            get { return _hashtag2.Replace("#", ""); }
            set
            {
                _hashtag2 = value;
                NotifyPropertyChange("Hashtag2");
                StartCommand.RaiseCanExecuteChanged();
            }
        }

        public DateTime StartDate
        {
            get { return _startDate; }
            set
            {
                _startDate = value;
                NotifyPropertyChange("StartDate");
            }
        }

        public DateTime EndDate
        {
            get { return _endDate; }
            set
            {
                _endDate = value.AddDays(1).AddSeconds(-1);
                NotifyPropertyChange("EndDate");
            }
        }

        public bool TimerEnable
        {
            get { return _timerEnable; }
            set
            {
                _timerEnable = value;
                NotifyPropertyChange("TimerEnable");
            }
        }

        public int Hours
        {
            get { return _hours; }
            set
            {
                _hours = value;
                NotifyPropertyChange("Hours");
            }
        }

        public int Minutes
        {
            get { return _minutes; }
            set
            {
                _minutes = value;
                NotifyPropertyChange("Minutes");
            }
        }

        public int Seconds
        {
            get { return _seconds; }
            set
            {
                _seconds = value;
                NotifyPropertyChange("Seconds");
            }
        }

        public bool BuildIgnoreList
        {
            get { return _buildIgnoreList; }
            set
            {
                _buildIgnoreList = value;
                NotifyPropertyChange("BuildIgnoreList");
            }
        }

        public DelegateCommand StartCommand { get; private set; }
        public DelegateCommand StopCommand { get; private set; }

        private void bgWrkInstagram_DoWork(object sender, DoWorkEventArgs e)
        {
            var download = !BuildIgnoreList;

            _instagram = new InstagramService(InstagramAccessTokenSingleton.Instance.AccessToken,
                DefaultPathSingleton.Instance.FolderPath, AppSettings.Get<int>("WebRequestTimeout"));
            _instagram.Start(Hashtag, Hashtag2, StartDate, EndDate, download);
        }

        private void bgWrkInstagram_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            EnableGui = true;
            Mediator.NotifyColleagues("Status", "Ready");
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if ((Hours == 0) && (Minutes == 0) && (Seconds == 0))
            {
                StopCommand.Execute(null);
            }
            else
            {
                if (Seconds <= 0)
                {
                    Seconds = 59;
                    if (Minutes == 0)
                    {
                        Minutes = 59;
                        if (Hours != 0)
                            Hours--;
                    }
                    else
                    {
                        Minutes--;
                    }
                }
                else
                {
                    Seconds--;
                }
            }
        }
    }
}