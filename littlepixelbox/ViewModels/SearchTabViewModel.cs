﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media.Imaging;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using littlepixelbox.Views;
using Newtonsoft.Json.Linq;
using NLog;

namespace littlepixelbox.ViewModels
{
    internal class SearchTabViewModel : ViewModelBase
    {
        public readonly int ImgHeight = 97;
        public readonly int ImgWidth = 97;

        private bool _enableGui = true;
        private bool _haveNextPageUrl;
        private readonly ObservableCollection<Image> _images = new ObservableCollection<Image>();

        private InstagramService _instagram;
        private JToken[] _json;

        private string _searchStatus = "Search for Users or Hashtags";

        private string _searchValue = string.Empty;
        private bool _tagSearch;
        private JObject _token;
        private bool _userSearch = true;
        private readonly Logger _log = LogManager.GetLogger("LittlePixelBox");

        private readonly BackgroundWorker _bgWrkLoadMore = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        private readonly BackgroundWorker _bgWrkSearchInstagram = new BackgroundWorker
        {
            WorkerReportsProgress = true
        };

        public SearchTabViewModel()
        {
            // Hook up background workers
            _bgWrkSearchInstagram.DoWork += bgWrkSearchInstagram_DoWork;
            _bgWrkSearchInstagram.RunWorkerCompleted += bgWrkSearchInstagram_RunWorkerCompleted;
            _bgWrkLoadMore.DoWork += bgWrkLoadMore_DoWork;
            _bgWrkLoadMore.RunWorkerCompleted += bgWrkLoadMore_RunWorkerCompleted;

            SearchCommand = new DelegateCommand(
                () =>
                {
                    if (string.IsNullOrEmpty(InstagramAccessTokenSingleton.Instance.AccessToken))
                    {
                        var il = new InstagramLogin();
                        il.ShowDialog();
                    }
                    else
                    {
                        var webReqestTimeout = !string.IsNullOrEmpty(AppSettings.Get<string>("WebRequestTimeout")) ? AppSettings.Get<int>("WebRequestTimeout") : 30000;
                        _instagram = new InstagramService(InstagramAccessTokenSingleton.Instance.AccessToken, string.Empty, webReqestTimeout);

                        EnableGui = false;
                        _bgWrkSearchInstagram.RunWorkerAsync();
                    }
                },
                () => !string.IsNullOrEmpty(SearchValue));
            PrintCommand = new DelegateCommand(
                PrintCurrentLayout,
                () => true);
            LoadMoreCommand = new DelegateCommand(
                () =>
                {
                    EnableGui = false;
                    _bgWrkLoadMore.RunWorkerAsync();
                },
                () => true);
        }        

        private void bgWrkSearchInstagram_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                SearchInstagram();
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void bgWrkSearchInstagram_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AddToImageList(_json);
            EnableGui = true;
        }

        private void bgWrkLoadMore_DoWork(object sender, DoWorkEventArgs e)
        {
            try
            {
                LoadMore();
            }
            catch (Exception ex)
            {
                _log.Error(ex.ToString());
                Mediator.NotifyColleagues("ErrorCount", 1);
                Mediator.NotifyColleagues("ErrorMessage", ex.Message);
            }
        }

        private void bgWrkLoadMore_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            AddToImageList(_json);
            EnableGui = true;
        }

        private void SearchInstagram()
        {
            SearchStatus = string.Empty;
            _instagram.NextPageUrl = null;

            if (UserSearch)
            {
                _token = _instagram.UserSearch(SearchValue);

                if (_token != null && !_token.SelectToken("data").ToString().Equals("[]"))
                {
                    _json = _token.SelectToken("data").ToArray();

                    foreach (var j in _json)
                    {
                        if (j.SelectToken("username").ToString().ToLower().Equals(SearchValue.ToLower()))
                        {
                            _token = _instagram.UserImageSearch(j.SelectToken("id").ToString());
                            _json = _token.SelectToken("data").ToArray();
                            SearchStatus = string.Empty;
                            break;
                        }
                        _json = null;
                    }

                    SearchStatus = _json != null ? string.Empty : "No users found.";
                }
                else
                {
                    _json = null;
                    SearchStatus = "No users found.";
                }
            }
            else if (TagSearch)
            {
                _token = _instagram.TagSearch(SearchValue);

                if (_token != null && !_token.SelectToken("data").ToString().Equals("[]"))
                {
                    _json = _token.SelectToken("data").ToArray();
                    SearchStatus = string.Empty;
                }
                else
                {
                    _json = null;
                    SearchStatus = "No hashtags found.";
                }
            }

            if (!string.IsNullOrEmpty(_instagram.NextPageUrl))
            {
                HaveNextPageUrl = true;
            }
        }

        private void PrintCurrentLayout()
        {
            // Notify MainViewModel to update the PolaroidModel Singleton
            Mediator.NotifyColleagues("SetPolaroidSingleton", true);
            // Get the PolaroidModel from the PolaroidSingleton
            var pm = PolaroidSingleton.Instance.PolaroidModel;

            var ps = new PolaroidService(pm);
            BitmapSource bitmap = ps.CreateBitmap(pm.Username.Text, pm.Caption.Text, pm.ProfileImage.Source,
                pm.Image.Source);

            PrinterSingleton.Instance.PrinterSettings.Copies = 1;

            var pps = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            pps.PrintImage("Little Pixel Box", Utils.BitmapFromSource(bitmap));
        }

        private void LoadMore()
        {
            if (!string.IsNullOrEmpty(_instagram.NextPageUrl))
            {
                _token = _instagram.GetToken((HttpWebRequest) WebRequest.Create(_instagram.NextPageUrl));
                _json = _token.SelectToken("data").ToArray();

                HaveNextPageUrl = _instagram.NextPageUrl != null;
            }
            else
            {
                HaveNextPageUrl = false;
            }
        }

        private void AddToImageList(JToken[] jt)
        {
            _images.Clear();

            if (jt != null)
            {
                foreach (var j in jt)
                {
                    var thumbnailUrl = j.SelectToken("images").SelectToken("thumbnail").SelectToken("url").ToString();

                    var img = new Image
                    {
                        Width = ImgWidth,
                        Height = ImgHeight,
                        Source = new BitmapImage(new Uri(thumbnailUrl, UriKind.Absolute))
                    };
                    img.MouseDown += selectedImage_MouseDown;

                    _images.Add(img);
                }
            }
        }

        public void selectedImage_MouseDown(object sender, MouseButtonEventArgs e)
        {
            var img = (Image) e.Source;

            foreach (var j in _json)
            {
                if (
                    j.SelectToken("images")
                        .SelectToken("thumbnail")
                        .SelectToken("url")
                        .ToString()
                        .Equals(img.Source.ToString()))
                {
                    var username = j.SelectToken("user").SelectToken("username").ToString();
                    var mediaId = j.SelectToken("id").ToString();
                    var profileImageUrl = j.SelectToken("user").SelectToken("profile_picture").ToString();
                    var mediaUrl =
                        j.SelectToken("images").SelectToken("standard_resolution").SelectToken("url").ToString();
                    var captionText = string.Empty;

                    if (j.SelectToken("caption").SelectToken("text") != null)
                    {
                        captionText = j.SelectToken("caption").SelectToken("text").ToString();
                    }

                    var d = new Dictionary<string, string>
                    {
                        {"username", username},
                        {"mediaId", mediaId},
                        {"profileImageUrl", profileImageUrl},
                        {"mediaUrl", mediaUrl},
                        {"captionText", captionText}
                    };

                    Mediator.NotifyColleagues("SetLayoutSources", d);
                }
            }
        }

        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public ObservableCollection<Image> Images
        {
            get { return _images; }
        }

        public string SearchValue
        {
            get { return _searchValue; }
            set
            {
                _searchValue = value.Trim();
                NotifyPropertyChange("SearchValue");
                SearchCommand.RaiseCanExecuteChanged();
            }
        }

        public string SearchStatus
        {
            get { return _searchStatus; }
            set
            {
                _searchStatus = value;
                NotifyPropertyChange("SearchStatus");
            }
        }

        public bool UserSearch
        {
            get { return _userSearch; }
            set
            {
                _userSearch = value;
                NotifyPropertyChange("UserSearch");
            }
        }

        public bool TagSearch
        {
            get { return _tagSearch; }
            set
            {
                _tagSearch = value;
                NotifyPropertyChange("TagSearch");
            }
        }

        public bool HaveNextPageUrl
        {
            get { return _haveNextPageUrl; }
            set
            {
                _haveNextPageUrl = value;
                NotifyPropertyChange("HaveNextPageUrl");
            }
        }

        public DelegateCommand SearchCommand { get; private set; }
        public DelegateCommand PrintCommand { get; private set; }
        public DelegateCommand LoadMoreCommand { get; private set; }
    }
}