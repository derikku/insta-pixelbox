﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Drawing.Printing;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using littlepixelbox.Core;
using littlepixelbox.Core.Helpers;
using littlepixelbox.Core.Singletons;
using littlepixelbox.Core.Utilities;
using NLog;

namespace littlepixelbox.ViewModels
{
    internal class PrinterTabViewModel : ViewModelBase
    {
        private bool _autoPrint;
        private readonly BackgroundWorker _bgWrkPrintService = new BackgroundWorker();
        private bool _enableGui = true;
        private int _marginX;
        private int _marginY;

        private string _pageOrientation;
        private readonly ObservableCollection<string> _pageOrientations = new ObservableCollection<string>();

        private PaperSize _paperSize;
        private readonly ObservableCollection<PaperSize> _paperSizes = new ObservableCollection<PaperSize>();
        private short _printCopies = 1;

        private string _printer;
        private readonly ObservableCollection<string> _printers = new ObservableCollection<string>();

        private readonly PrinterSettings _printerSettings = new PrinterSettings();

        private PrintPolaroidService _printService;

        private readonly DispatcherTimer _timerPrintService = new DispatcherTimer();
        private readonly Logger _log = LogManager.GetLogger("PrintingService");


        public PrinterTabViewModel()
        {
            Mediator.Register("RunPrintPolaroidService", RunPrintPolaroidService);

            // Hook up the timer
            _timerPrintService.Tick += timerPrintService_Tick;
            _timerPrintService.Interval = TimeSpan.FromSeconds(1);

            // Hook up background worker
            _bgWrkPrintService.DoWork += bgWrkPrintService_DoWork;

            _log.Trace("Finding all Printers...");
            foreach (string ps in PrinterSettings.InstalledPrinters)
            {
                _printers.Add(ps);
                _log.Trace("Added: " + ps);
            }

            // Select default printer
            Printer = _printerSettings.PrinterName;

            // Add page orientations
            PageOrientations.Add("Landscape");
            PageOrientations.Add("Portrait");

            SyncDefaultPrinterSettings();

            PrintCurrentLayoutCommand = new DelegateCommand(
                TestPrint,
                () => true);

            PrintPreviewCommand = new DelegateCommand(
                PrintPreview,
                () => true);
        }

        private void SyncDefaultPrinterSettings()
        {
            _paperSizes.Clear();

            // Set the selected printer
            _printerSettings.PrinterName = Printer;

            // Add printers supported media sizes
            _log.Trace("Finding printer Media Size capabilities...");
            if (_printerSettings.PaperSizes.Count != 0)
            {
                // Add printers supported paper sizes
                _log.Trace("Finding printer Paper Sizes...");
                foreach (PaperSize size in _printerSettings.PaperSizes)
                {
                    _paperSizes.Add(size);
                }

                // Select the default paper size
                _log.Trace("Finding default Paper Size...");
                foreach (var ps in _paperSizes)
                {
                    if (_printerSettings.DefaultPageSettings.PaperSize.PaperName.Equals(ps.PaperName))
                    {
                        PaperSize = ps;
                        _log.Trace("Default Page Size: " + ps.PaperName);
                        break;
                    }
                }
            }

            // Set the default orientation
            PageOrientation = _printerSettings.DefaultPageSettings.Landscape ? "Landscape" : "Portrait";
        }

        public void RunPrintPolaroidService(object args)
        {
            EnableGui = false;

            if ((bool) args)
            {
                if (AutoPrint)
                {
                    _timerPrintService.Start();
                    _log.Info("Printing Service started.");
                }
            }
            else
            {
                _timerPrintService.Stop();
                _log.Info("Printing Service stopped.");
                EnableGui = true;
            }
        }

        private void timerPrintService_Tick(object sender, EventArgs e)
        {
            if (!_bgWrkPrintService.IsBusy)
            {
                _bgWrkPrintService.RunWorkerAsync();
            }
        }

        private void bgWrkPrintService_DoWork(object sender, DoWorkEventArgs e)
        {
            _printService = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            _printService.PrintCreatedImages();
        }

        private void PrintPreview()
        {
            _printService = new PrintPolaroidService(PrinterSingleton.Instance.PrinterSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            _printService.PrintPreview(Utils.BitmapFromSource(LayoutToBitmapSource()));
        }

        private void TestPrint()
        {
            var printerSettings = PrinterSingleton.Instance.PrinterSettings;
            printerSettings.Copies = 1;

            _printService = new PrintPolaroidService(printerSettings,
                PrinterSingleton.Instance.MarginX,
                PrinterSingleton.Instance.MarginY);

            _printService.PrintImage("Little Pixel Box", Utils.BitmapFromSource(LayoutToBitmapSource()));
        }

        private BitmapSource LayoutToBitmapSource()
        {
            // Notify MainViewModel to update the PolaroidModel Singleton
            Mediator.NotifyColleagues("SetPolaroidSingleton", true);

            // Get the PolaroidModel from the PolaroidSingleton
            var pm = PolaroidSingleton.Instance.PolaroidModel;

            var ps = new PolaroidService(pm);
            BitmapSource bitmap = ps.CreateBitmap(pm.Username.Text, pm.Caption.Text, pm.ProfileImage.Source,
                pm.Image.Source);

            return bitmap;
        }



        public bool EnableGui
        {
            get { return _enableGui; }
            set
            {
                _enableGui = value;
                NotifyPropertyChange("EnableGui");
            }
        }

        public bool AutoPrint
        {
            get { return _autoPrint; }
            set
            {
                _autoPrint = value;
                NotifyPropertyChange("AutoPrint");
            }
        }

        public string Printer
        {
            get { return _printer; }
            set
            {
                _printer = value;
                PrinterSingleton.Instance.PrinterSettings.PrinterName = _printer;
                SyncDefaultPrinterSettings();
                NotifyPropertyChange("Printer");
            }
        }

        public ObservableCollection<string> Printers
        {
            get { return _printers; }
        }

        public PaperSize PaperSize
        {
            get { return _paperSize; }
            set
            {
                _paperSize = value;
                PrinterSingleton.Instance.PrinterSettings.DefaultPageSettings.PaperSize = value;
                NotifyPropertyChange("PaperSize");
            }
        }

        public ObservableCollection<PaperSize> PaperSizes
        {
            get { return _paperSizes; }
        }

        public string PageOrientation
        {
            get { return _pageOrientation; }
            set
            {
                _pageOrientation = value;
                PrinterSingleton.Instance.PrinterSettings.DefaultPageSettings.Landscape = value.Equals("Landscape");
                NotifyPropertyChange("PageOrientation");
            }
        }

        public ObservableCollection<string> PageOrientations
        {
            get { return _pageOrientations; }
        }

        public short PrintCopies
        {
            get { return _printCopies; }
            set
            {
                _printCopies = value;
                PrinterSingleton.Instance.PrinterSettings.Copies = value;
                NotifyPropertyChange("PrintCopies");
            }
        }

        public int MarginX
        {
            get { return _marginX; }
            set
            {
                _marginX = value;
                PrinterSingleton.Instance.MarginX = value;
                NotifyPropertyChange("MarginX");
            }
        }

        public int MarginY
        {
            get { return _marginY; }
            set
            {
                _marginY = value;
                PrinterSingleton.Instance.MarginY = value;
                NotifyPropertyChange("MarginY");
            }
        }

        public DelegateCommand PrintPreviewCommand { get; private set; }
        public DelegateCommand PrintCurrentLayoutCommand { get; private set; }
    }
}