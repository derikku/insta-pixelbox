﻿using System.Windows;
using System.Windows.Controls;

namespace littlepixelbox.Designer.MoveResizeControls
{
    public class ResizeChrome : Control
    {
        static ResizeChrome()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(ResizeChrome), new FrameworkPropertyMetadata(typeof(ResizeChrome)));
        }
    }
}