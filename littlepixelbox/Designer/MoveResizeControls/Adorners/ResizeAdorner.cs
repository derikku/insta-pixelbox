﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;

namespace littlepixelbox.Designer.MoveResizeControls.Adorners
{
    public class ResizeAdorner : Adorner
    {
        private readonly ContentControl _designerItem;
        private readonly ResizeChrome _chrome;
        private readonly VisualCollection _visuals;

        public ResizeAdorner(ContentControl designerItem) : base(designerItem)
        {
            _designerItem = designerItem;
            SnapsToDevicePixels = true;
            _chrome = new ResizeChrome {DataContext = designerItem};
            _visuals = new VisualCollection(this) {_chrome};
        }

        protected override int VisualChildrenCount
        {
            get { return _visuals.Count; }
        }

        protected override Visual GetVisualChild(int index)
        {
            return _visuals[index];
        }

        protected override Size ArrangeOverride(Size arrangeBounds)
        {
            _chrome.Arrange(new Rect(arrangeBounds));
            return arrangeBounds;
        }
    }
}