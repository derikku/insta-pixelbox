﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;

namespace littlepixelbox.Designer.MoveResizeControls.Thumbs
{
    public class MoveThumb : Thumb
    {
        private Canvas _canvas;
        private ContentControl _designerItem;

        public MoveThumb()
        {
            DragStarted += MoveThumb_DragStarted;
            DragDelta += MoveThumb_DragDelta;
        }

        private void MoveThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            _designerItem = DataContext as ContentControl;
            if (_designerItem != null) _canvas = VisualTreeHelper.GetParent(_designerItem) as Canvas;
        }

        private void MoveThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            if (_designerItem != null)
            {
                var dragDelta = new Point(e.HorizontalChange, e.VerticalChange);

                var nx = Math.Min(Math.Max(Canvas.GetLeft(_designerItem) + dragDelta.X, 0),
                    _canvas.Width - _designerItem.Width);
                var ny = Math.Min(Math.Max(Canvas.GetTop(_designerItem) + dragDelta.Y, 0),
                    _canvas.Height - _designerItem.Height);

                Canvas.SetLeft(_designerItem, nx);
                Canvas.SetTop(_designerItem, ny);
            }
        }
    }
}