﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using littlepixelbox.Designer.MoveResizeControls.Adorners;
using NLog;

namespace littlepixelbox.Designer.MoveResizeControls.Thumbs
{
    public class ResizeThumb : Thumb
    {
        private Adorner _adorner;
        private readonly double _angle;
        private Canvas _canvas;
        private ContentControl _designerItem;
        private Point _transformOrigin;

        public ResizeThumb()
        {
            DragStarted += ResizeThumb_DragStarted;
            DragDelta += ResizeThumb_DragDelta;
            DragCompleted += ResizeThumb_DragCompleted;
            _angle = 0.0d;
        }

        /// <summary>
        ///     Handles notifications when the dragging of the thumb starts.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResizeThumb_DragStarted(object sender, DragStartedEventArgs e)
        {
            _designerItem = DataContext as ContentControl;

            if (_designerItem != null)
            {
                _canvas = VisualTreeHelper.GetParent(_designerItem) as Canvas;

                if (_canvas != null)
                {
                    _transformOrigin = _designerItem.RenderTransformOrigin;

                    var adornerLayer = AdornerLayer.GetAdornerLayer(_canvas);
                    if (adornerLayer != null)
                    {
                        _adorner = new SizeAdorner(_designerItem);
                        adornerLayer.Add(_adorner);
                    }
                }
            }
        }

        /// <summary>
        ///     Handles notifications when the thumb has been dragged.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResizeThumb_DragDelta(object sender, DragDeltaEventArgs e)
        {
            ResizeItem(e, Keyboard.IsKeyDown(Key.LeftShift));
        }


        /// <summary>
        ///     Dragging method for resize items.
        /// </summary>
        /// <param name="e"></param>
        /// <param name="keepAspectRatio"></param>
        private void ResizeItem(DragDeltaEventArgs e, bool keepAspectRatio)
        {
            var minLeft = Canvas.GetLeft(_designerItem);
            var minTop = Canvas.GetTop(_designerItem);
            var maxRight = _canvas.Width - (Canvas.GetLeft(_designerItem) + _designerItem.Width);
            var maxBottom = _canvas.Height - (Canvas.GetTop(_designerItem) + _designerItem.Height);

            var minDeltaVertical = _designerItem.ActualHeight - _designerItem.MinHeight;
            var minDeltaHorizontal = _designerItem.ActualWidth - _designerItem.MinWidth;
            var maxDeltaVertical = _canvas.Width;
            var maxDeltaHorizontal = _canvas.Width;

            double? dragDeltaVertical = null;
            switch (VerticalAlignment)
            {
                case VerticalAlignment.Top:
                    dragDeltaVertical = Math.Min(Math.Max(-minTop, e.VerticalChange), minDeltaVertical);
                    dragDeltaVertical = _designerItem.ActualHeight - dragDeltaVertical > 0 ? dragDeltaVertical : 0;
                    break;
                case VerticalAlignment.Bottom:
                    dragDeltaVertical = Math.Min(Math.Max(-maxBottom, -e.VerticalChange), maxDeltaVertical);
                    dragDeltaVertical = _designerItem.ActualHeight - dragDeltaVertical > 0 ? dragDeltaVertical : 0;
                    break;
            }

            double? dragDeltaHorizontal = null;
            switch (HorizontalAlignment)
            {
                case HorizontalAlignment.Left:
                    dragDeltaHorizontal = Math.Min(Math.Max(-minLeft, e.HorizontalChange), minDeltaHorizontal);
                    dragDeltaHorizontal = _designerItem.ActualWidth - dragDeltaHorizontal > 0 ? dragDeltaHorizontal : 0;
                    break;
                case HorizontalAlignment.Right:
                    dragDeltaHorizontal = Math.Min(Math.Max(-maxRight, -e.HorizontalChange), maxDeltaHorizontal);
                    dragDeltaHorizontal = _designerItem.ActualWidth - dragDeltaHorizontal > 0 ? dragDeltaHorizontal : 0;
                    break;
            }

            // in case the aspect ratio is kept then adjust both width and height
            if (keepAspectRatio)
            {
                CheckAspectRatio(ref dragDeltaHorizontal, ref dragDeltaVertical,
                    _designerItem.ActualHeight/_designerItem.ActualWidth);
            }

            if (dragDeltaVertical.HasValue)
            {
                switch (VerticalAlignment)
                {
                    case VerticalAlignment.Bottom:
                        Canvas.SetTop(_designerItem,
                            Canvas.GetTop(_designerItem) +
                            _transformOrigin.Y*dragDeltaVertical.Value*(1 - Math.Cos(-_angle)));
                        Canvas.SetLeft(_designerItem,
                            Canvas.GetLeft(_designerItem) - dragDeltaVertical.Value*_transformOrigin.Y*Math.Sin(-_angle));
                        break;
                    case VerticalAlignment.Top:
                        Canvas.SetTop(_designerItem,
                            Canvas.GetTop(_designerItem) + dragDeltaVertical.Value*Math.Cos(-_angle) +
                            _transformOrigin.Y*dragDeltaVertical.Value*(1 - Math.Cos(-_angle)));
                        Canvas.SetLeft(_designerItem,
                            Canvas.GetLeft(_designerItem) + dragDeltaVertical.Value*Math.Sin(-_angle) -
                            _transformOrigin.Y*dragDeltaVertical.Value*Math.Sin(-_angle));
                        break;
                    case VerticalAlignment.Center:
                        break;
                    case VerticalAlignment.Stretch:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                var height = _designerItem.ActualHeight - dragDeltaVertical.Value;
                _designerItem.Height = height > 0 ? height : 0;
            }

            if (dragDeltaHorizontal.HasValue)
            {
                switch (HorizontalAlignment)
                {
                    case HorizontalAlignment.Left:
                        Canvas.SetTop(_designerItem, Canvas.GetTop(_designerItem) + dragDeltaHorizontal.Value*Math.Sin(_angle) - _transformOrigin.X*dragDeltaHorizontal.Value*Math.Sin(_angle));
                        Canvas.SetLeft(_designerItem, Canvas.GetLeft(_designerItem) + dragDeltaHorizontal.Value*Math.Cos(_angle) + _transformOrigin.X*dragDeltaHorizontal.Value*(1 - Math.Cos(_angle)));
                        break;
                    case HorizontalAlignment.Right:
                        Canvas.SetTop(_designerItem, Canvas.GetTop(_designerItem) - _transformOrigin.X*dragDeltaHorizontal.Value*Math.Sin(_angle));
                        Canvas.SetLeft(_designerItem, Canvas.GetLeft(_designerItem) + dragDeltaHorizontal.Value*_transformOrigin.X*(1 - Math.Cos(_angle)));
                        break;
                    case HorizontalAlignment.Center:
                        break;
                    case HorizontalAlignment.Stretch:
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }

                var width = _designerItem.ActualWidth - dragDeltaHorizontal.Value;
                _designerItem.Width = width > 0 ? width : 0;
            }

            e.Handled = true;
        }

        /// <summary>
        ///     Checks the values so that the ratio between them has a defined value.
        /// </summary>
        /// <param name="dragDeltaHorizontal">horizontal delta</param>
        /// <param name="dragDeltaVertical">vertical delta</param>
        /// <param name="aspectRatio">horizontal to vertical ration</param>
        public void CheckAspectRatio(ref double? dragDeltaHorizontal, ref double? dragDeltaVertical, double aspectRatio)
        {
            double? dragValue = null;
            if (dragDeltaVertical.HasValue && dragDeltaHorizontal.HasValue)
            {
                dragValue = Math.Max(dragDeltaVertical.Value, dragDeltaHorizontal.Value);
            }
            else if (dragDeltaVertical.HasValue)
            {
                dragValue = dragDeltaVertical;
            }
            else if (dragDeltaHorizontal.HasValue)
            {
                dragValue = dragDeltaHorizontal;
            }

            if (dragValue.HasValue)
            {
                dragDeltaVertical = dragValue.Value*aspectRatio;
                dragDeltaHorizontal = dragValue;
            }
        }

        /// <summary>
        ///     Handles notifications when the dragging of the thumb completes.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ResizeThumb_DragCompleted(object sender, DragCompletedEventArgs e)
        {
            if (_adorner != null)
            {
                var adornerLayer = AdornerLayer.GetAdornerLayer(_canvas);
                if (adornerLayer != null)
                {
                    adornerLayer.Remove(_adorner);
                }
                _adorner = null;
            }
        }
    }
}