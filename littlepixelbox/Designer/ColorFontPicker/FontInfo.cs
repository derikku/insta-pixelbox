﻿using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace littlepixelbox.Designer.ColorFontPicker
{
    public class FontInfo
    {
        public FontInfo(FontFamily fam, double sz, FontStyle style, FontStretch strc, FontWeight weight,
            SolidColorBrush c)
        {
            Family = fam;
            Size = sz;
            Style = style;
            Stretch = strc;
            Weight = weight;
            BrushColor = c;
        }

        public FontFamily Family { get; set; }
        public double Size { get; set; }
        public FontStyle Style { get; set; }
        public FontStretch Stretch { get; set; }
        public FontWeight Weight { get; set; }
        public SolidColorBrush BrushColor { get; set; }

        public FamilyTypeface Typeface
        {
            get
            {
                var ftf = new FamilyTypeface
                {
                    Stretch = Stretch,
                    Weight = Weight,
                    Style = Style
                };
                return ftf;
            }
        }


        public static string TypefaceToString(FamilyTypeface ttf)
        {
            var sb = new StringBuilder(ttf.Stretch.ToString());
            sb.Append("-");
            sb.Append(ttf.Weight);
            sb.Append("-");
            sb.Append(ttf.Style);
            return sb.ToString();
        }

        public static void ApplyTextBlockFont(TextBlock textBlock, FontInfo font)
        {
            textBlock.FontFamily = font.Family;
            textBlock.FontSize = font.Size;
            textBlock.FontStyle = font.Style;
            textBlock.FontStretch = font.Stretch;
            textBlock.FontWeight = font.Weight;
            textBlock.Foreground = font.BrushColor;
        }

        public static FontInfo GetTextBlockFont(TextBlock textBlock)
        {
            var font = new FontInfo(textBlock.FontFamily,
                textBlock.FontSize,
                textBlock.FontStyle,
                textBlock.FontStretch,
                textBlock.FontWeight,
                (SolidColorBrush) textBlock.Foreground);

            return font;
        }
    }
}