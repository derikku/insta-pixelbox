﻿using System.Linq;
using System.Windows;
using System.Windows.Media;

namespace littlepixelbox.Designer.ColorFontPicker
{
    /// <summary>
    ///     Interaction logic for ColorFontDialog.xaml
    /// </summary>
    public partial class ColorFontDialog
    {
        private FontInfo _selectedFont;

        public ColorFontDialog()
        {
            _selectedFont = null; // Default
            InitializeComponent();
        }

        public FontInfo SelectedFont
        {
            get
            {
                return new FontInfo(txtSampleText.FontFamily,
                    txtSampleText.FontSize,
                    txtSampleText.FontStyle,
                    txtSampleText.FontStretch,
                    txtSampleText.FontWeight,
                    new SolidColorBrush(Color.FromArgb(colorPicker.SelectedColor.A,
                        colorPicker.SelectedColor.R, colorPicker.SelectedColor.G,
                        colorPicker.SelectedColor.B)));
            }
        }


        public FontInfo Font
        {
            get { return _selectedFont; }

            set
            {
                var fi = value;
                _selectedFont = fi;
            }
        }

        private void SyncFontName()
        {
            var fontFamilyName = _selectedFont.Family.Source;
            var idx = (from object item in lstFamily.Items select item.ToString()).TakeWhile(itemName => fontFamilyName != itemName).Count();
            lstFamily.SelectedIndex = idx;
            lstFamily.ScrollIntoView(lstFamily.Items[idx]);
        }

        private void SyncFontSize()
        {
            var fontSize = _selectedFont.Size;
            fontSizeSlider.Value = fontSize;
        }

        private void SyncFontColor()
        {
            colorPicker.SelectedColor = Font.BrushColor.Color;
            txtSampleText.Foreground = Font.BrushColor;
        }

        private void SyncFontTypeface()
        {
            var fontTypeFaceSb = FontInfo.TypefaceToString(_selectedFont.Typeface);
            var idx = (from object item in lstTypefaces.Items select item as FamilyTypeface).TakeWhile(face => fontTypeFaceSb != FontInfo.TypefaceToString(face)).Count();
            lstTypefaces.SelectedIndex = idx;
        }

        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            Font = SelectedFont;
            DialogResult = true;
        }

        private void Window_Loaded_1(object sender, RoutedEventArgs e)
        {
            SyncFontColor();
            SyncFontName();
            SyncFontSize();
            SyncFontTypeface();
        }

        private void colorPicker_ColorChanged(object sender, RoutedPropertyChangedEventArgs<Color> e)
        {
            txtSampleText.Foreground = new SolidColorBrush(Color.FromArgb(colorPicker.SelectedColor.A,
                colorPicker.SelectedColor.R, colorPicker.SelectedColor.G, colorPicker.SelectedColor.B));
        }
    }
}