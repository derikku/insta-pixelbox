var imgPerRow = 5;
var imgMargin = 0;
var scrollSpeed = 30;
var getImageTimer = 3000; //miliseconds

$("body").height($(window).height());
$(window).resize(function() {
    $("body").height($(window).height());
});

var hub = $.connection.collageHub;

$.connection.hub.start(function() {
    hub.server.getCollageSettings();

    setInterval(function () {
        hub.server.getImages();
    }, getImageTimer);
}).done(function() {
}).fail(function () {
    console.log("SignalR: Could not connect ...");
});

hub.client.settings = function(i, s) {
    imgPerRow = i;
    scrollSpeed = s * 1.5;
};

function DiplayedImages() {
    var imgs = [];

    $(".collage img").each(function (i, img) {
        imgs.push(img.src.substring(img.src.lastIndexOf("/") + 1, img.src.length));
    });
    return imgs;
};

hub.client.imageList = function (images, reload) {
    if (reload) {
        location.reload();
    } else {
        for (var i = 0; i < images.length; i++) {
            if ($.inArray(images[i].substring(images[i].lastIndexOf("\\") + 1, images[i].length), DiplayedImages()) === -1) {
                var imgWidth = ($(window).width() / imgPerRow) - (imgMargin * 2);
                var img = $('<img src="' + images[i] + '" width="' + imgWidth + '"style="margin:' + imgMargin + 'px"/>').hide().fadeIn(1000);
                $(".collage").append(img);
            }
        };
    };

    var marquee = $("body");
    marquee.marquee();
};

(function($) {
    $(".collage").css("margin-top", $(window).height());
    var methods = {
        init: function(options) {
            this.children(":first").stop();
            this.marquee("play");
        },
        play: function() {
            var marquee = this,
                pixelsPerSecond = scrollSpeed,
                firstChild = this.children(":first"),
                totalHeight = 0;

            // Find the total height of the children by adding each child's height:
            this.children().each(function(index, element) {
                totalHeight += $(element).innerHeight();
            });

            // The distance the divs have to travel to reach -1 * totalHeight:
            var difference = totalHeight + parseInt(firstChild.css("margin-top"), 10);

            // The duration of the animation needed to get the correct speed:
            var duration = (difference / pixelsPerSecond) * 1000;

            // Animate the first child's margin-top to -1 * totalHeight:
            firstChild.animate(
                { 'margin-top': -1 * totalHeight },
                duration,
                "linear",
                function() {
                    // Move the first child back down (below the screen):
                    firstChild.css("margin-top", $(window).height());
                    // Restart whole process... :)
                    marquee.marquee("play");
                }
            );
        },
        pause: function() {
            this.children(":first").stop();
        }
    };

    $.fn.marquee = function(method) {
        // Method calling logic
        if (methods[method]) {
            return methods[method].apply(this, Array.prototype.slice.call(arguments, 1));
        } else if (typeof method === "object" || !method) {
            return methods.init.apply(this, arguments);
        } else {
            $.error("Method " + method + " does not exist on jQuery.marquee");
            return null;
        }
    };

})(jQuery);


//marquee.hover(function () {
//    marquee.marquee('pause');
//}, function () {
//    marquee.marquee('play');
//});