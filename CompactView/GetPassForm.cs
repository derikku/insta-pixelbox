﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace CompactView
{
    public partial class GetPassForm : Form
    {
        public GetPassForm()
        {
            InitializeComponent();
            SetCultureTexts();

            this.ActiveControl = edPass;
            btOK.Focus();
        }

        private void SetCultureTexts()
        {
            this.Text = GlobalText.GetValue("Password");
            label1.Text = GlobalText.GetValue("PasswordNote");
            btOK.Text = GlobalText.GetValue("Ok");
            btCancel.Text = GlobalText.GetValue("Cancel");
        }
    }
}
